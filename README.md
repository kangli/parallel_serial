This is the repository for the manuscript: 

__Kang Li__, Mikiko Kadohisa, Makoto Kusunoki, John Duncan, Claus Bundesen, Susanne Ditlevsen: *Distinguishing between parallel and serial processing in visual attention from neurobiological data* (2018)

#### Content
- `data`: spike train data in `MatLab` data format
- `include`: header files
- `src`: source files
- `programs`: code that performs all the analysis

#### How to use
The executable for the analysis can be built with CMake:
```sh
mkdir build
cd build
cmake ..
make
```

#### Requirements
- `boost` (http://www.boost.org/)
- `nlopt` (http://ab-initio.mit.edu/wiki/index.php/NLopt)
- `gsl` (https://www.gnu.org/software/gsl/)
- `R` (https://www.r-project.org/)
- `matio` (https://github.com/tbeu/matio)
