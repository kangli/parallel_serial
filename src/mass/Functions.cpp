#include <mass/Functions.h>

namespace {

void get_spike_train_boundaries(
        double s, double e, 
        const std::vector<double> st, 
        double& bound1, double& bound2) {
    double b1, b2;
    b1 = e - s;
    b2 = 0;
    for (auto i : st) {
        if (i >= s && i <= e) {
            b1 = i - s;
            break;
        }
    }
    int size = st.size();
    if (size > 0) {
        for (int i = size-1; i >= 0; --i) {
            if (st[i] <= e && st[i] >= s) {
                b2 = e - st[i];
                break;
            }
        }
    }
    bound1 = b1;
    bound2 = b2;
}

} // unnamed namespace

namespace mass {

double UnbiasedResponseKernel::calc(
        double start, double now, const std::vector<double>& st) {
    double h = 0;
    for (size_t i = 0; i < st.size(); ++i) {
        double s = st[i];
        int n = round( (now - s) / 0.001 );
        if (n > m_num) {
            continue;
        } else if (n <= 0) {
            break;
        } else {
            h += m_params[n-1];
        }
    }
    return h;
}

double ExponentialResponseKernel::calc(
        double start, double now, const std::vector<double>& st) {
    double h = 0;
    /*
    double a1 = m_params[0];
    double b1 = m_params[1];
    double a2 = m_params[2];
    double b2 = m_params[3];
    */
    size_t sz = st.size();
    for (size_t i = 0; i < sz; ++i) {
        double s = st[i];
        if (s <= start) {
            double tmp = now - s;
            size_t id = tmp/0.0001;
            //h += a1 * exp(b1 * tmp) + a2 * exp(b2 * tmp);
            if (id <= m_s && id >= 0) {
                h += m_vals[id];
            }
        } else {
            break;
        }
    }
    return h;
}

int ExponentialResponseKernel::set_parameters(
        int s, const std::vector<double>& params) {
    int i = ResponseKernel::set_parameters(s, params);
    double a1 = m_params[0];
    double b1 = m_params[1];
    double a2 = m_params[2];
    double b2 = m_params[3];
    m_vals.clear();
    int c = 0;
    for (double d = 0; c < 100 && d < 5; d+=0.0001) {
        double v = a1 * exp(b1 * d) + a2 * exp(b2 * d);
        m_vals.push_back(v);
        if (v < 1e-5 && v > -1e-5) {
            c++;
        } else {
            c = 0;
        }
    }
    m_s = m_vals.size();
    return i;
}

double LogGammaResponseKernel::calc(
        double start, double now, const std::vector<double>& st) {
    double alpha = m_params[0];
    double beta = m_params[1];
    double sy = m_params[3];
    double sx = m_params[4];
    double h = 0;
    for (size_t i = 0; i < st.size(); ++i) {
        double s = st[i];
        if (s <= start) {
            double tmp = now - s;
            if (tmp <= 0) tmp = 1e-5;
            tmp *= sx;
            h += sy * log( 1 + (m_c * pow(tmp, alpha-1) - 1) * exp(-beta*tmp) );
        } else {
            break;
        }
    }
    return h;
}

std::vector<double> LogGammaResponseKernel::test(
        double from, double to, int n) {
    std::vector<double> r;
    double alpha = m_params[0];
    double beta = m_params[1];
    double sx = m_params[3];
    double sy = m_params[4];
    for(double i = from; i < to; i+=(to-from)/n) {
        double j = i*sy;
        r.push_back(sx * log( 1 + (m_c * pow(j, alpha-1) - 1) * exp(-beta*j) ));
    }
    return r;
}


double ISIDistributionExponential::calc_log_spike_train(
        double s, double e, const std::vector<double>& st, double lam) {
    int n = 0;
    for (auto i : st) {
        if (i < e && i > s) {
            n++;
        }
    }
    return (n+1) * log(lam) - (e - s) * lam;
}

void ISIDistributionExponential::calc_cdf_values(
        const std::vector<double>& st, double start, double end,
        double lam, std::vector<double>& result) {
    std::vector<double> valid, isi;
    for (double i : st) {
        if (i > start && i < end)
            valid.push_back(i);
    }
    if (valid.size() == 0) {
        isi.push_back(end - start);
    } else if (valid.size() == 1) {
        isi.push_back(valid[0] - start);
        isi.push_back(end - valid[0]);
    } else {
        isi.push_back(valid[0] - start);
        for (size_t ind = 1; ind < isi.size(); ++ind) {
            isi.push_back(valid[ind] - valid[ind-1]);
        }
        isi.push_back(end - valid.back());
    }
    for (double x : isi) {
        double r = calc_cdf(x, lam);
        result.push_back(r);
    }
}

double ISIDistributionExponential::calc_count_cdf(const std::vector<double>& st, 
        double start, double end, double lam) {
    int n = 0;
    for (auto i : st) {
        if (i > end) break;
        if (i >= start) n++;
    }
    if (n < 0) return 0;
    double lam_m = lam * (end-start);
    return boost::math::gamma_q(n+1, lam_m);
}

double ISIDistributionExponential::calc_count_cdf_adjust(
        const std::vector<double>& st, 
        double start, double end, double lam) {
    int n = 0;
    for (auto i : st) {
        if (i > end) break;
        if (i >= start) n++;
    }
    if (n <= 0) return 0;
    double lam_m = lam * (end-start);
    double r1 = boost::math::gamma_q(n+1, lam_m);
    double r2 = boost::math::gamma_q(n, lam_m);
    return (r1 + r2) * 0.5;
}

void ISIDistributionGamma::calc_cdf_values(
        const std::vector<double>& st, double start, double end,
        double lam, std::vector<double>& result) {
    std::vector<double> valid, isi;
    double bound1, bound2;
    for (double i : st) {
        if (i >= start && i <= end)
            valid.push_back(i);
    }
    if (valid.size() > 1) {
        for (size_t j = 1; j < valid.size(); ++j) {
            isi.push_back(valid[j] - valid[j-1]);
        }
    }
    get_spike_train_boundaries(start, end, st, bound1, bound2);
    for (double x : isi) {
        double r = calc_cdf(x, lam);
        result.push_back(r);
    }
    if (bound1 != 0) {
        double fw = 0;
        int max = round(bound1 / m_dt);
        for (int j = 0; j < max; ++j) {
            fw += (1 - calc_cdf((j+0.5)*m_dt, lam)) * m_dt;
        }
        fw *= lam;
        result.push_back(fw);
    }
    if (bound2 != 0) {
        double fw = 0;
        int max = round(bound2 / m_dt);
        for (int j = 0; j < max; ++j) {
            fw += (1 - calc_cdf((j+0.5)*m_dt, lam)) * m_dt;
        }
        fw *= lam;
        result.push_back(fw);
    }
}

double ISIDistributionGamma::calc_log_spike_train(
        double s, double e, const std::vector<double>& st, double lam) {
    int n = 0;
    double log_sum = 0;
    std::vector<double> valid;
    for (double i : st) {
        if (i <= e && i >= s) {
            n++;
            valid.push_back(i);
        } else if (i > e) break;
    }
    for (size_t j = 1; j < valid.size(); ++j) {
        log_sum += log( (valid[j]-valid[j-1]) );
    }
    double beta = m_gamma * lam;
    double result = m_gamma * n * log(beta) +
        (m_gamma - 1) * log_sum -
        beta * (e - s) -
        n * ::lgamma(m_gamma);
    double bound1, bound2;
    get_spike_train_boundaries(s, e, st, bound1, bound2);
    if (bound1 != 0) {
        result += log(lam) + log_1_minus_cdf(lam, bound1);
    }
    if (bound2 != 0) {
        result += log(lam) + log_1_minus_cdf(lam, bound2);
    }
    return result;
}

double ISIDistributionGamma::calc_count_cdf(const std::vector<double>& st, 
        double start, double end, double lam) {
    int n = 0;
    for (auto i : st) {
        if (i > end) break;
        if (i >= start) n++;
    }
    if (n < 0) return 0;
    double lam_m = lam * (end - start);
    double bt = m_gamma * lam_m;
    double an1 = m_gamma * n + m_gamma;
    return 1 - boost::math::gamma_p(an1, bt);
}

double ISIDistributionGamma::calc_count_cdf_adjust(
        const std::vector<double>& st, 
        double start, double end, double lam) {
    int n = 0;
    for (auto i : st) {
        if (i > end) break;
        if (i >= start) n++;
    }
    if (n <= 0) return 0;
    double lam_m = lam * (end - start);
    double bt = m_gamma * lam_m;
    double an1 = m_gamma * n + m_gamma;
    double an2 = m_gamma * n;
    double r1 = 1 - boost::math::gamma_p(an1, bt);
    double r2 = 1 - boost::math::gamma_p(an2, bt);
    return (r1 + r2) * 0.5;
}


void ISIDistributionInvGauss::calc_cdf_values(
        const std::vector<double>& st, double start, double end,
        double lam, std::vector<double>& result) {
    std::vector<double> valid, isi;
    double bound1, bound2;
    for (double i : st) {
        if (i >= start && i <= end)
            valid.push_back(i);
    }
    if (valid.size() > 1) {
        for (size_t j = 1; j < valid.size(); ++j) {
            isi.push_back(valid[j] - valid[j-1]);
        }
    }
    get_spike_train_boundaries(start, end, st, bound1, bound2);
    for (double x : isi) {
        double r = calc_cdf(x, lam);
        result.push_back(r);
    }
    if (bound1 != 0) {
        double fw = 0;
        int max = round(bound1 / m_dt);
        for (int j = 0; j < max; ++j) {
            fw += (1 - calc_cdf((j+0.5)*m_dt, lam)) * m_dt;
        }
        fw *= lam;
        result.push_back(fw);
    }
    if (bound2 != 0) {
        double fw = 0;
        int max = round(bound2 / m_dt);
        for (int j = 0; j < max; ++j) {
            fw += (1 - calc_cdf((j+0.5)*m_dt, lam)) * m_dt;
        }
        fw *= lam;
        result.push_back(fw);
    }
}

double ISIDistributionInvGauss::calc_log_spike_train(
        double s, double e, const std::vector<double>& st, double lam) {
    int n = 0;
    double log_sum = 0, sum_1_over = 0;
    std::vector<double> valid;
    for (double i : st) {
        if (i <= e && i >= s) {
            n++;
            valid.push_back(i);
        } else if (i > e) break;
    }
    for (size_t j = 1; j < valid.size(); ++j) {
        double len = valid[j] - valid[j-1];
        log_sum += log( len );
        sum_1_over += 1.0/len;
    }
    double mu = 1.0 / lam;
    double result = n / 2.0 * log(m_var) - n/2.0*log(2*M_PI) - 
        1.5 * log_sum -
        m_var / 2.0 / mu / mu * (e-s) +
        n * m_var / mu - m_var / 2.0 * sum_1_over;
    double bound1, bound2;
    get_spike_train_boundaries(s, e, st, bound1, bound2);
    if (bound1 != 0) {
        result += log(lam) + log_1_minus_cdf(lam, bound1);
    }
    if (bound2 != 0) {
        result += log(lam) + log_1_minus_cdf(lam, bound2);
    }
    return result;
}

double ISIDistributionInvGauss::calc_count_cdf(const std::vector<double>& st, 
        double start, double end, double lam) {
    int n = 0;
    for (auto i : st) {
        if (i > end) break;
        if (i >= start) n++;
    }
    if (n < 0) return 0;
    double mu = 1.0/lam * (n + 1);
    double lambda = m_var * (n + 1) * (n + 1);
    double t = (end - start);
    boost::math::inverse_gaussian ig(mu, lambda);
    return 1 - boost::math::cdf(ig, t);
}

double ISIDistributionInvGauss::calc_count_cdf_adjust(
        const std::vector<double>& st, 
        double start, double end, double lam) {
    int n = 0;
    for (auto i : st) {
        if (i > end) break;
        if (i >= start) n++;
    }
    if (n <= 0) return 0;
    double mu1 = 1.0/lam * (n + 1);
    double mu2 = 1.0/lam * (n);
    double lambda1 = m_var * (n + 1) * (n + 1);
    double lambda2 = m_var * (n) * (n);
    double t = (end - start);
    boost::math::inverse_gaussian ig1(mu1, lambda1);
    boost::math::inverse_gaussian ig2(mu2, lambda2);
    double r1 = 1 - boost::math::cdf(ig1, t);
    double r2 = 1 - boost::math::cdf(ig2, t);
    return 0.5 * (r1 + r2);
}

std::shared_ptr<ResponseKernel> create_response_kernel(FuncType ft, int n) {
    std::shared_ptr<ResponseKernel> f;
    switch (ft) {
        case RK_NON :
            f = std::shared_ptr<NoneResponseKernel>(new NoneResponseKernel(n));
            break;
        case RK_UNB :
            f = std::shared_ptr<UnbiasedResponseKernel>(
                    new UnbiasedResponseKernel(n));
            break;
        case RK_EXP:
            f = std::shared_ptr<ExponentialResponseKernel>(
                    new ExponentialResponseKernel);
            break;
        case RK_LGA:
            f = std::shared_ptr<LogGammaResponseKernel>(
                    new LogGammaResponseKernel);
            break;
        default:
            f = std::shared_ptr<UnbiasedResponseKernel>(
                    new UnbiasedResponseKernel(n));

    }
    f->set_parameter_names();
    return f;
}

std::shared_ptr<StimulusKernel> create_stimulus_kernel(FuncType ft, int n) {
    std::shared_ptr<StimulusKernel> f;
    switch (ft) {
        case SK_SMP :
            f = std::shared_ptr<SimpleStimulusKernel>(
                    new SimpleStimulusKernel(n));
            break;
        case SK_EMP :
            f = std::shared_ptr<EmptyStimulusKernel>(
                    new EmptyStimulusKernel(n));
            break;
        case SK_PW :
            f = std::shared_ptr<PWStimulusKernel>(
                    new PWStimulusKernel(n));
            break;
        case SK_INPUT :
            f = std::shared_ptr<InputStimulusKernel>(
                    new InputStimulusKernel(n));
            break;
        case SK_SIN :
            f = std::shared_ptr<SinusoidalStimulusKernel>(
                    new SinusoidalStimulusKernel(n));
            break;
        default:
            f = std::shared_ptr<SimpleStimulusKernel>(
                    new SimpleStimulusKernel(n));
    }
    f->set_parameter_names();
    return f;
}

std::shared_ptr<PerceptionKernel> create_perception_kernel(FuncType ft, int n) {
    std::shared_ptr<PerceptionKernel> f;
    switch (ft) {
        case PK_GAUSS :
            f = std::shared_ptr<GaussianPerceptionKernel>(
                    new GaussianPerceptionKernel(n));
            break;
        case PK_GAUSS_INT_MAP :
            f = std::shared_ptr<GaussianIntMapPerceptionKernel>(
                    new GaussianIntMapPerceptionKernel(n));
            break;
        default:
            f = std::shared_ptr<GaussianPerceptionKernel>(
                    new GaussianPerceptionKernel(n));
    }
    f->set_parameter_names();
    return f;
}

std::shared_ptr<ISIDistribution> create_isi_distribution(FuncType ft) {
    std::shared_ptr<ISIDistribution> f;
    switch (ft) {
        case ISI_EXP :
            f = std::shared_ptr<ISIDistribution>(
                    new ISIDistributionExponential);
            break;
        case ISI_GAM :
            f = std::shared_ptr<ISIDistribution>(
                    new ISIDistributionGamma);
            break;
        case ISI_IG :
            f = std::shared_ptr<ISIDistribution>(
                    new ISIDistributionInvGauss);
            break;
        default:
            f = std::shared_ptr<ISIDistribution>(
                    new ISIDistributionExponential);
    }
    f->set_parameter_names();
    return f;
}

std::shared_ptr<Distance> create_distance_method(FuncType ft) {
    std::shared_ptr<Distance> f;
    switch (ft) {
        case DIST_DTW_ST :
            f = std::shared_ptr<DistanceDTWST>(
                    new DistanceDTWST);
            break;
        case DIST_DTW_ISI :
            f = std::shared_ptr<DistanceDTWISI>(
                    new DistanceDTWISI);
            break;
        case DIST_ISI_DENS :
            f = std::shared_ptr<DistanceISIDensity>(
                    new DistanceISIDensity);
            break;
        case DIST_VM :
            f = std::shared_ptr<DistanceVanRossum>(
                    new DistanceVanRossum);
            break;
        default:
            f = std::shared_ptr<DistanceDTWST>(
                    new DistanceDTWST);
    }
    return f;
}

}
