#include <project/NeuralData.h>
#include <project/Serialization.h>
#include <project/HHMM.h>
#include <project/CB.h>

using namespace std;
using namespace project;

struct Result {
    double min_f;
    int count;
    vector<double> params;
};

typedef map<string, Result> Results;

namespace boost {
namespace serialization {

template <class Archive>
void serialize(Archive & ar, Result& d, const unsigned int version) {
    ar & d.min_f;
    ar & d.count;
    ar & d.params;
}

}
}

std::vector<double> make_phase(double from, double to, int l) {
    double step = (to - from) / l;
    double t = from;
    std::vector<double> v = {from};
    for (int i = 0; i < l-1; ++i) {
        t += step;
        v.push_back(t);
        v.push_back(t);
    }
    v.push_back(to);
    return v;
}

// different phase settings
vector<double> g_phase_4_ = {0.7, 0.1, 0.1, 0.2, 0.2, 0.3, 0.3, 0.4, 0.4, 0.5};
vector<double> g_phase_3 = {0.1, 0.233, 0.233, 0.366, 0.366, 0.5};
vector<double> g_phase_4 = {0.1, 0.2, 0.2, 0.3, 0.3, 0.4, 0.4, 0.5};
vector<double> g_phase_5 = {0.1, 0.18, 0.18, 0.26, 0.26, 0.34, 0.34, 0.42, 0.42, 0.5};
vector<double> g_phase_10 = make_phase(0.1, 0.5, 10);

vector<double> g_phase_sim = {0, 0.1, 0.1, 0.2, 0.2, 0.3, 0.3, 0.4, 0.4, 0.5, 0.5, 0.6, 0.6, 0.7, 0.7, 0.8, 0.8, 0.9, 0.9, 1.0};
vector<double> g_phase_sim2 = {0, 0.1, 0.1, 0.2, 0.2, 0.3, 0.3, 0.4, 0.4, 0.5};

// condition settings
vector<vector<Obj>> g_conditions_10 = {
    {OBJ_NONE, OBJ_T},
    {OBJ_T,    OBJ_NONE},
    {OBJ_NI,   OBJ_T,
     OBJ_NC,   OBJ_T},
    {OBJ_T,    OBJ_NI,
     OBJ_T,    OBJ_NC},
    {OBJ_NONE, OBJ_NI},
    {OBJ_NONE, OBJ_NC},
    {OBJ_NI,   OBJ_NONE},
    {OBJ_NC,   OBJ_NONE},
    {OBJ_NC,   OBJ_NI},
    {OBJ_NI,   OBJ_NC},
};

vector<vector<Obj>> g_conditions_12 = {
    {OBJ_NONE, OBJ_T},
    {OBJ_T,    OBJ_NONE},
    {OBJ_NI,   OBJ_T},
    {OBJ_NC,   OBJ_T},
    {OBJ_T,    OBJ_NI},
    {OBJ_T,    OBJ_NC},
    {OBJ_NONE, OBJ_NI},
    {OBJ_NONE, OBJ_NC},
    {OBJ_NI,   OBJ_NONE},
    {OBJ_NC,   OBJ_NONE},
    {OBJ_NC,   OBJ_NI},
    {OBJ_NI,   OBJ_NC},
};

vector<vector<Obj>> g_conditions_3 = {
    {OBJ_T,    OBJ_NONE,
     OBJ_T,    OBJ_NI,
     OBJ_T,    OBJ_NC},
    {OBJ_NONE, OBJ_T,
     OBJ_NI,   OBJ_T,
     OBJ_NC,   OBJ_T},
    {OBJ_NONE, OBJ_NI,
     OBJ_NONE, OBJ_NC,
     OBJ_NI,   OBJ_NONE,
     OBJ_NC,   OBJ_NONE,
     OBJ_NC,   OBJ_NI,
     OBJ_NI,   OBJ_NC},
};

vector<vector<Obj>> g_conditions_1 = {
    {OBJ_T,    OBJ_NONE,
     OBJ_T,    OBJ_NI,
     OBJ_T,    OBJ_NC,
     OBJ_NONE, OBJ_T,
     OBJ_NI,   OBJ_T,
     OBJ_NC,   OBJ_T,
     OBJ_NONE, OBJ_NI,
     OBJ_NONE, OBJ_NC,
     OBJ_NI,   OBJ_NONE,
     OBJ_NC,   OBJ_NONE,
     OBJ_NC,   OBJ_NI,
     OBJ_NI,   OBJ_NC},
};

// simulationg parameters
std::vector<std::vector<double>> lams = {
    {0.3, 0.65, 0.05},
    {0.5, 0.4, 0.1},
    {0.7, 0.15, 0.15}
};

std::vector<std::vector<std::vector<double>>> gams = {
    {
        {0.95, 0.02, 0.03},
        {0.1, 0.8, 0.1},
        {0.5, 0.2, 0.3}
    },
    {
        {0.3, 0.6, 0.1},
        {0.1, 0.3, 0.6},
        {0.01, 0.01, 0.98}
    },
    {
        {0.2, 0.1, 0.7},
        {0.05, 0.9, 0.05},
        {0.7, 0.1, 0.2}
    }
};

std::vector<std::vector<double>> A = {
    {0.91, 0.09},
    {0.45, 0.55},
    {0.15, 0.85}
};

std::vector<std::vector<double>> rhos = {
    {0.6, 0.6, 0.3, 0.15, 0.1},
    {0.2, 0.2, 0.3, 0.4, 0.4},
    {0.3, 0.3, 0.4, 0.45, 0.5}
};

std::vector<std::vector<double>> ps = {
    {0.7, 0.65, 0.72, 0.75, 0.75},
    {0.65, 0.7, 0.75, 0.8, 0.8},
    {0.6, 0.55, 0.4, 0.3, 0.3}
};

void save_data(const std::vector<double>& phase) {
    NeuralData nd;
    nd.set_phase(phase);
    nd.set_cue(0);
    nd.set_spike_num_thres(2);
    nd.read_data();
    save<AllData>(nd.get_data(), "mkdata.dat");
    save<SumStat>(nd.get_sum_stat(), "mksumstat.dat");
    save<map<string, int>>(nd.get_neuron_number(), "mknum.dat");
}

void read_data(NeuralData& nd) {
    AllData ad;
    SumStat ss;
    map<string, int> nn;
    load<AllData>(ad, "mkdata.dat");
    load<SumStat>(ss, "mksumstat.dat");
    load<map<string, int>>(nn, "mknum.dat");
    nd.set_data(ad);
    nd.set_sumstat(ss);
    nd.set_neuron_number(nn);
}

void read_data(std::string& name, NeuralData& nd) {
    AllData ad;
    SumStat ss;
    map<string, int> nn;
    load<AllData>(ad, "mkdata.dat");
    load<SumStat>(ss, "mksumstat.dat");
    load<map<string, int>>(nn, "mknum.dat");
    AllData nad;
    SumStat nss;
    map<string, int> nnn;
    nad[name] = (ad)[name];
    nss[name] = (ss)[name];
    nnn[name] = (nn)[name];
    nd.set_data(nad);
    nd.set_sumstat(nss);
    nd.set_neuron_number(nnn);
}

// print in format read into R
void print_data(const std::vector<double>& phase) {
    NeuralData nd;
    nd.set_phase(phase);
    nd.set_cue(0);
    nd.read_data();
    std::ofstream of("mkdataR.txt");
    for (auto& d : *(nd.get_data()) ) {
        for (auto& e : d.second) {
            for (DataMK& mk : e.second) {
                of << d.first <<"\t" << e.first << "\t" << mk.name << "\t" << mk.index
                   << "\t" << mk.condition << "\t" << mk.cue << "\t" << mk.contra
                   << "\t" << mk.ipsi << "\t";
                for (auto s : mk.spike) {
                    of << s << "\t";
                }
                of << std::endl;
            }
        }
    }
}

// print in format read into R
void print_data(NeuralData& nd, const string& file) {
    std::ofstream of(file);
    for (auto& d : *(nd.get_data()) ) {
        for (auto& e : d.second) {
            for (DataMK& mk : e.second) {
                of << d.first <<"\t" << e.first << "\t" << mk.name << "\t" << mk.index
                   << "\t" << mk.condition << "\t" << mk.cue << "\t" << mk.contra
                   << "\t" << mk.ipsi << "\t";
                for (auto s : mk.spike) {
                    of << s << "\t";
                }
                of << std::endl;
            }
        }
    }
}

// print in format read into R
void print_data(const string& datfile, const string& file) {
    AllData ad;
    load<AllData>(ad, datfile);
    std::ofstream of(file);
    for (auto& d : ad ) {
        for (auto& e : d.second) {
            for (DataMK& mk : e.second) {
                of << d.first <<"\t" << e.first << "\t" << mk.name << "\t" << mk.index
                   << "\t" << mk.condition << "\t" << mk.cue << "\t" << mk.contra
                   << "\t" << mk.ipsi << "\t";
                for (auto s : mk.spike) {
                    of << s << "\t";
                }
                of << std::endl;
            }
        }
    }
}

void loadprint_one(const string& input, const string& output) {
    Result result;
    load<Result>(result, input);
    ofstream of(output);
    of << result.min_f << "\t" << result.count;
    for (double v : result.params) {
        of << "\t" << v;
    }
    of << std::endl;
}

void loadprint(const string& input, const string& output) {
    NeuralData nd;

    std::map<std::string, std::vector<std::string>> file_map = nd.get_file_map(nd.get_file_list());
    std::map<std::string, int> neuron_number = *(nd.get_neuron_number());
    Results results;
    load<Results>(results, input);
    ofstream of(output);
    for (auto& r: results) {
        of << r.first << "\t" << r.second.min_f << "\t" << r.second.count << "\t" << neuron_number[r.first];
        for (double v : r.second.params) {
            of << "\t" << v;
        }
        of << std::endl;
    }
}


// Fit a Binomial HMM model
Result run(const string& name, NeuralData& nd, const std::vector<double>& phase, vector<vector<Obj>> conditions, std::vector<Result>& results, int cue = 1, bool initial_flag = true, int rep = 50, std::string file = "unnamed") {
    AllData nad;
    SumStat nss;
    map<string, int> nnn;
    nad[name] = (*(nd.get_data()))[name];
    nnn[name] = (*(nd.get_neuron_number()))[name];
    nss = *(nd.get_sum_stat());

    HHMM hhmm(name, &nad, &nss, &nnn,
            mass::RK_UNB, 10, mass::PK_GAUSS_INT_MAP, 3);
    hhmm.set_cue(cue);
    hhmm.set_initial_flag(initial_flag);
    //hhmm.set_cb(cb);
    std::map<std::string, std::vector<std::string>> file_map = 
        nd.get_file_map(nd.get_file_list());

    double min_f = HUGE_VAL;
    Result opt_result;

    for (int rep_i = 0; rep_i < rep; ++rep_i) {
        // gammas parameter
        std::vector<double> gammas(9*conditions.size(), 0);
        hhmm.randomize_params_set(gammas, -5, 5);
        std::vector<double> gammas_lb(9*conditions.size(), -10);
        std::vector<double> gammas_ub(9*conditions.size(), 10);
        for (size_t i = 0; i < conditions.size(); ++i) {
            gammas_lb[2+i*9] = 0;
            gammas_lb[5+i*9] = 0;
            gammas_lb[8+i*9] = 0;
            gammas_ub[2+i*9] = 0;
            gammas_ub[5+i*9] = 0;
            gammas_ub[8+i*9] = 0;
        }
        // lambda parameter
        int lsize = initial_flag ? 1 : conditions.size();
        std::vector<double> lambdas(3*lsize, 0);
        hhmm.randomize_params_set(lambdas, -5, 5);
        std::vector<double> lambdas_lb(3*lsize, -10);
        std::vector<double> lambdas_ub(3*lsize, 10);
        for (size_t i = 0; i < lsize; ++i) {
            lambdas_lb[2+i*3] = 0;
            lambdas_ub[2+i*3] = 0;
        }
        // rate
        std::vector<double> rates, rates_lb, rates_ub;
        hhmm.get_rate_params_from_data(file_map[name], rates, rates_lb, rates_ub);
        hhmm.randomize_params_mul(rates, 0.5, 2.0);

        // cif
        std::vector<double> cif(10, 0), cif_lb(10, -10), cif_ub(10, 10);
        hhmm.randomize_params_set(cif, -1, 1);

        hhmm.set_up(
                file_map[name],
                // phase    
                phase,
                conditions,
                //lambda
                lambdas,
                lambdas_lb,
                lambdas_ub,
                // gamma
                gammas,
                gammas_lb, 
                gammas_ub,
                // A
                {0.2, 0.5, 0.8},
                {0.001, 0.334, 0.667},
                {0.333, 0.666, 0.999},
                // rate
                rates, rates_lb, rates_ub,
                0, 0, 0,
                // cif
                cif,
                cif_lb, 
                cif_ub);
        hhmm.set_maxeval(20000);
        // initial values
        //hhmm.print_parameters();
        hhmm.optimize();
        // optimized values
        hhmm.print_parameters();
        
        auto one_result = Result{hhmm.get_min_f(), hhmm.get_count(), hhmm.get_parameters()};

        save<Result>(one_result, 
                file + "_" + name + "_rep_" + std::to_string(rep_i) + ".dat");
        loadprint_one(
                file + "_" + name + "_rep_" + std::to_string(rep_i) + ".dat",
                file + "_" + name + "_rep_" + std::to_string(rep_i) + ".txt");

        double new_min_f = hhmm.get_min_f();
        if (new_min_f < min_f) {
            min_f = new_min_f;
            opt_result = one_result;
        }
        results.push_back(one_result);
    }
    return opt_result;
}

// fit a correlated Binomial model
Result run_cb(const string& name, NeuralData& nd, const std::vector<double>& phase, vector<vector<Obj>> conditions, std::vector<Result>& results, int cue = 1, int rep = 50, std::string file = "unnamed") {
    AllData nad;
    SumStat nss;
    map<string, int> nnn;
    nad[name] = (*(nd.get_data()))[name];
    nnn[name] = (*(nd.get_neuron_number()))[name];
    nss = *(nd.get_sum_stat());

    CB cb(name, &nad, &nss, &nnn,
            mass::RK_UNB, 10, mass::PK_GAUSS_INT_MAP, 3);
    cb.set_cue(cue);
    std::map<std::string, std::vector<std::string>> file_map = 
        nd.get_file_map(nd.get_file_list());

    double min_f = HUGE_VAL;
    Result opt_result;

    for (int rep_i = 0; rep_i < rep; ++rep_i) {
        // rate
        std::vector<double> rates, rates_lb, rates_ub;
        cb.get_rate_params_from_data(file_map[name], rates, rates_lb, rates_ub);
        cb.randomize_params_mul(rates, 0.5, 2.0);

        // rho
        std::vector<double> rho(conditions.size() * (phase.size()/2 - 1) + 1, 0);
        std::vector<double> rho_lb(conditions.size() * (phase.size()/2 - 1) + 1, 0.001);
        std::vector<double> rho_ub(conditions.size() * (phase.size()/2 - 1) + 1, 0.999);
        cb.randomize_params_set(rho, 0.1, 0.9);

        // p
        std::vector<double> pp(conditions.size() * (phase.size()/2 - 1) + 1, 0);
        std::vector<double> pp_lb(conditions.size() * (phase.size()/2 - 1) + 1, 0.001);
        std::vector<double> pp_ub(conditions.size() * (phase.size()/2 - 1) + 1, 0.999);
        cb.randomize_params_set(pp, 0.1, 0.9);

        // cif
        std::vector<double> cif(10, 0), cif_lb(10, -10), cif_ub(10, 10);
        cb.randomize_params_set(cif, -1, 1);

        cb.set_up(
                file_map[name],
                // phase    
                phase,
                conditions,
                // rho
                rho, rho_lb, rho_ub,
                // p
                pp, pp_lb, pp_ub,
                // rate
                rates, rates_lb, rates_ub,
                0, 0, 0,
                // cif
                cif, cif_lb, cif_ub);
        cb.set_maxeval(20000);
        // initial values
        //cb.print_parameters();
        cb.optimize();
        // optimized values
        cb.print_parameters();

        auto one_result = Result{cb.get_min_f(), cb.get_count(), cb.get_parameters()};

        save<Result>(one_result, 
                file + "_" + name + "_rep_" + std::to_string(rep_i) + ".dat");
        loadprint_one(
                file + "_" + name + "_rep_" + std::to_string(rep_i) + ".dat",
                file + "_" + name + "_rep_" + std::to_string(rep_i) + ".txt");

        double new_min_f = cb.get_min_f();
        if (new_min_f < min_f) {
            min_f = new_min_f;
            opt_result = one_result;
        }
        results.push_back(one_result);
    }

    return opt_result;
}



// decode Neuroal attention using the Binomial HMM model
void run_decode(const string& name, NeuralData& nd, Result& result, const std::vector<double>& phase, vector<vector<Obj>> conditions, int cue = 1) {
    AllData nad;
    SumStat nss;
    map<string, int> nnn;
    nad[name] = (*(nd.get_data()))[name];
    nnn[name] = (*(nd.get_neuron_number()))[name];
    nss = *(nd.get_sum_stat());

    HHMM hhmm(name, &nad, &nss, &nnn,
            mass::RK_UNB, 10, mass::PK_GAUSS_INT_MAP, 3);
    hhmm.set_cue(cue);
    std::map<std::string, std::vector<std::string>> file_map = 
        nd.get_file_map(nd.get_file_list());
    hhmm.set_up(
        file_map[name],
        phase,
        conditions,
        result.params);
    hhmm.decode_attention();

}

// decode using the correlated Binomial model
void run_decode_cb(const string& name, NeuralData& nd, Result& result, const std::vector<double>& phase, vector<vector<Obj>> conditions, int cue = 1) {
    AllData nad;
    SumStat nss;
    map<string, int> nnn;
    nad[name] = (*(nd.get_data()))[name];
    nnn[name] = (*(nd.get_neuron_number()))[name];
    nss = *(nd.get_sum_stat());

    CB cb(name, &nad, &nss, &nnn,
            mass::RK_UNB, 10, mass::PK_GAUSS_INT_MAP, 3);
    cb.set_cue(cue);
    std::map<std::string, std::vector<std::string>> file_map = 
        nd.get_file_map(nd.get_file_list());
    cb.set_up(
        file_map[name],
        phase,
        conditions,
        result.params);
    cb.decode_attention();

}


// read data and fit the Binomial HMM model for all sessions
void fit_hmm(const std::vector<double>& phase, const std::vector<std::vector<Obj>>& conditions, const std::string& file, bool initial_flag = true) {
    Results results;
    Results all_results;

    NeuralData nd;
    nd.set_phase(phase);
    nd.set_cue(0);
    nd.set_spike_num_thres(2);
    nd.read_data();

    std::map<std::string, std::vector<std::string>> file_map = nd.get_file_map(nd.get_file_list());
    std::map<std::string, int> neuron_number = *(nd.get_neuron_number());

    std::vector<std::string> keys;
    for (auto& nn : neuron_number) {
        keys.push_back(nn.first);
    }

#ifdef ENABLE_OPENMP
        omp_lock_t writelock;
        omp_init_lock(&writelock);
#endif


#ifdef ENABLE_OPENMP
#pragma omp parallel for schedule (dynamic)
#endif
    for (size_t kid = 0; kid < keys.size(); ++kid) {
        auto& nn_first = keys[kid];
        auto& nn_second = neuron_number[nn_first];
        if (nn_second >= 5) {
            std::cout << "# " << nn_first << " " << nn_second << std::endl;
            std::vector<Result> all_result;
            Result result = run(nn_first, nd, phase, conditions, all_result, 0, initial_flag, 10, file);
#ifdef ENABLE_OPENMP
            omp_set_lock(&writelock);
#endif
            results[nn_first] = result;
            for (int rep_i = 0; rep_i < 10; ++ rep_i) {
                all_results[nn_first + "_" + std::to_string(rep_i)] = all_result[rep_i];
            }
#ifdef ENABLE_OPENMP
            omp_unset_lock(&writelock);
#endif
        }
    }
    save<Results>(results, file + ".optimal.dat");
    loadprint(file + ".optimal.dat", file + ".optimal.txt");
    save<Results>(all_results, file + ".all.dat");
    loadprint(file + ".all.dat", file + ".all.txt");

#ifdef ENABLE_OPENMP
    omp_destroy_lock(&writelock);
#endif
}

// read data and fit the correlated Binomial model for all sessions
void fit_cb(const std::vector<double>& phase, const std::vector<std::vector<Obj>>& conditions, const std::string& file) {
    Results results;
    Results all_results;

    NeuralData nd;
    nd.set_phase(phase);
    nd.set_cue(0);
    nd.set_spike_num_thres(2);
    nd.read_data();

    std::map<std::string, std::vector<std::string>> file_map = nd.get_file_map(nd.get_file_list());
    std::map<std::string, int> neuron_number = *(nd.get_neuron_number());

    std::vector<std::string> keys;
    for (auto& nn : neuron_number) {
        keys.push_back(nn.first);
    }

#ifdef ENABLE_OPENMP
        omp_lock_t writelock;
        omp_init_lock(&writelock);
#endif


#ifdef ENABLE_OPENMP
#pragma omp parallel for schedule (dynamic)
#endif
    for (size_t kid = 0; kid < keys.size(); ++kid) {
        auto& nn_first = keys[kid];
        auto& nn_second = neuron_number[nn_first];
        if (nn_second >= 5) {
            std::cout << "# " << nn_first << " " << nn_second << std::endl;
            std::vector<Result> all_result;
            Result result = run_cb(nn_first, nd, phase, conditions, all_result, 0, 10, file);
#ifdef ENABLE_OPENMP
            omp_set_lock(&writelock);
#endif
            results[nn_first] = result;
            for (int rep_i = 0; rep_i < 10; ++ rep_i) {
                all_results[nn_first + "_" + std::to_string(rep_i)] = all_result[rep_i];
            }
#ifdef ENABLE_OPENMP
            omp_unset_lock(&writelock);
#endif
        }
    }
    save<Results>(results, file + ".optimal.dat");
    loadprint(file + ".optimal.dat", file + ".optimal.txt");
    save<Results>(all_results, file + ".all.dat");
    loadprint(file + ".all.dat", file + ".all.txt");


#ifdef ENABLE_OPENMP
    omp_destroy_lock(&writelock);
#endif
}


// read data and decode using the Binomial HMM for all data
void decode_hmm(const std::vector<double>& phase, const std::vector<std::vector<Obj>>& conditions, const std::string& input) {
    Results results;
    load<Results>(results, input);

    NeuralData nd;
    nd.set_phase(phase);
    nd.set_cue(0);
    nd.set_spike_num_thres(2);
    //std::string name = "MN110118";
    nd.read_data();

    std::map<std::string, std::vector<std::string>> file_map = nd.get_file_map(nd.get_file_list());
    std::map<std::string, int> neuron_number = *(nd.get_neuron_number());

    for (auto& nn : neuron_number) {
        if (nn.second >= 5) {
            std::cout << "# " << nn.first << " " << nn.second << std::endl;
            run_decode(nn.first, nd, results[nn.first], phase, conditions, 0);
        }
    }
}

// read data and decode using the correlated Binomial model for all data
void decode_cb(const std::vector<double>& phase, const std::vector<std::vector<Obj>>& conditions, const std::string& input) {
    Results results;
    load<Results>(results, input);

    NeuralData nd;
    nd.set_phase(phase);
    nd.set_cue(0);
    nd.set_spike_num_thres(2);
    std::string name = "MN110118";
    nd.read_data();

    std::map<std::string, std::vector<std::string>> file_map = nd.get_file_map(nd.get_file_list());
    std::map<std::string, int> neuron_number = *(nd.get_neuron_number());

    for (auto& nn : neuron_number) {
        if (nn.second >= 5) {
            std::cout << "# " << nn.first << " " << nn.second << std::endl;
            run_decode_cb(nn.first, nd, results[nn.first], phase, conditions, 0);
        }
    }
}

void run_simulation(Trials& trials, Result& result, const std::vector<double>& lam, const std::vector<std::vector<double>>& gam, const std::vector<std::vector<double>>& A) {
    HHMM hhmm("simulation", mass::RK_UNB, 10, mass::PK_GAUSS_INT_MAP, 3);
    hhmm.set_cue(0);
    AllData ad = hhmm.simulate_data(
        gam, lam, A,
        {80, 0, 0, 0, 20, 0, 0,
         80, 0, 0, 0, 10, 0, 0,
         80, 0, 0, 0, 30, 0, 0,
         90, 0, 0, 0, 10, 0, 0,
         60, 0, 0, 0, 30, 0, 0,
         70, 0, 0, 0, 20, 0, 0,
         80, 0, 0, 0, 10, 0, 0,
         70, 0, 0, 0, 20, 0, 0,
         50, 0, 0, 0, 10, 0, 0,
         40, 0, 0, 0, 20, 0, 0},
         0,
        {-2, -0.5, 0, 0.5, 0.2, 0.1, 0.1, 0.1, 0.05, 0.05},
        g_phase_sim,
        g_conditions_1,
        10, 20, 10
    );
    map<string, int> nnn;
    nnn["sim"] = 10;
    HHMM hmm_fit("sim", &ad, NULL, &nnn,
            mass::RK_UNB, 10, mass::PK_GAUSS_INT_MAP, 3);
    hmm_fit.set_cue(0);
    hmm_fit.set_up({},
            g_phase_sim,
            g_conditions_1,
            {
                0, 0, 0,
                0,0,0,0,0,0,0,0,0,
                0.8,0.2,0.5,
                50, 0, 0, 0, 10, 0, 0,
                50, 0, 0, 0, 10, 0, 0,
                50, 0, 0, 0, 10, 0, 0,
                50, 0, 0, 0, 10, 0, 0,
                50, 0, 0, 0, 10, 0, 0,
                50, 0, 0, 0, 10, 0, 0,
                50, 0, 0, 0, 10, 0, 0,
                50, 0, 0, 0, 10, 0, 0,
                50, 0, 0, 0, 10, 0, 0,
                50, 0, 0, 0, 10, 0, 0,
                0,
                0, 0, 0,0,0,0,0,0,0,0
            });
    hmm_fit.set_lower_bounds({
                -10, -10, -10,
                -10,-10,-10,-10,-10,-10,-10,-10,-10,
                0.7,1e-10,0.4,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0,
                -10, -10, -10,-10,-10,-10,-10,-10,-10,-10
            });
    hmm_fit.set_upper_bounds({
                10, 10, 10,
                10,10,10,10,10,10,10,10,10,
                1-1e-10,0.3,0.6,
                200, 0, 0, 0, 200, 0, 0,
                200, 0, 0, 0, 200, 0, 0,
                200, 0, 0, 0, 200, 0, 0,
                200, 0, 0, 0, 200, 0, 0,
                200, 0, 0, 0, 200, 0, 0,
                200, 0, 0, 0, 200, 0, 0,
                200, 0, 0, 0, 200, 0, 0,
                200, 0, 0, 0, 200, 0, 0,
                200, 0, 0, 0, 200, 0, 0,
                200, 0, 0, 0, 200, 0, 0,
                0,
                10,10,10,10,10,10,10,10,10,10
            });
    hmm_fit.set_maxeval(100000);
    hmm_fit.optimize();
    //hmm_fit.print_parameters();

    trials = ad["sim"];
    result = Result{hmm_fit.get_min_f(), hmm_fit.get_count(), hmm_fit.get_parameters()};
}

void run_simulation_cb(Trials& trials, Result& result, const std::vector<double>& rho, const std::vector<double>& p) {
    CB cb("simulation", mass::RK_UNB, 10, mass::PK_GAUSS_INT_MAP, 3);
    cb.set_cue(0);
    AllData ad = cb.simulate_data(
        rho, p,
        {80, 0, 0, 0, 20, 0, 0,
         80, 0, 0, 0, 10, 0, 0,
         80, 0, 0, 0, 30, 0, 0,
         90, 0, 0, 0, 10, 0, 0,
         60, 0, 0, 0, 30, 0, 0,
         70, 0, 0, 0, 20, 0, 0,
         80, 0, 0, 0, 10, 0, 0,
         70, 0, 0, 0, 20, 0, 0,
         50, 0, 0, 0, 10, 0, 0,
         40, 0, 0, 0, 20, 0, 0},
         0,
        {-2, -0.5, 0, 0.5, 0.2, 0.1, 0.1, 0.1, 0.05, 0.05},
        g_phase_sim2,
        g_conditions_1,
        5, 20, 10
    );
    map<string, int> nnn;
    nnn["sim"] = 10;
    CB cb_fit("sim", &ad, NULL, &nnn,
            mass::RK_UNB, 10, mass::PK_GAUSS_INT_MAP, 3);
    cb_fit.set_cue(0);
    cb_fit.set_up({},
            g_phase_sim2,
            g_conditions_1,
            {
                0.5, 0.5, 0.5, 0.5, 0.5,
                0.5, 0.5, 0.5, 0.5, 0.5,
                50, 0, 0, 0, 10, 0, 0,
                50, 0, 0, 0, 10, 0, 0,
                50, 0, 0, 0, 10, 0, 0,
                50, 0, 0, 0, 10, 0, 0,
                50, 0, 0, 0, 10, 0, 0,
                50, 0, 0, 0, 10, 0, 0,
                50, 0, 0, 0, 10, 0, 0,
                50, 0, 0, 0, 10, 0, 0,
                50, 0, 0, 0, 10, 0, 0,
                50, 0, 0, 0, 10, 0, 0,
                0,
                0, 0, 0,0,0,0,0,0,0,0
            });
    cb_fit.set_lower_bounds({
                1e-10, 1e-10, 1e-10, 1e-10, 1e-10,
                1e-10, 1e-10, 1e-10, 1e-10, 1e-10,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0,
                -10, -10, -10,-10,-10,-10,-10,-10,-10,-10
            });
    cb_fit.set_upper_bounds({
                1-1e-10, 1-1e-10, 1-1e-10, 1-1e-10, 1-1e-10,
                1-1e-10, 1-1e-10, 1-1e-10, 1-1e-10, 1-1e-10,
                200, 0, 0, 0, 200, 0, 0,
                200, 0, 0, 0, 200, 0, 0,
                200, 0, 0, 0, 200, 0, 0,
                200, 0, 0, 0, 200, 0, 0,
                200, 0, 0, 0, 200, 0, 0,
                200, 0, 0, 0, 200, 0, 0,
                200, 0, 0, 0, 200, 0, 0,
                200, 0, 0, 0, 200, 0, 0,
                200, 0, 0, 0, 200, 0, 0,
                200, 0, 0, 0, 200, 0, 0,
                0,
                10,10,10,10,10,10,10,10,10,10
            });
    cb_fit.set_maxeval(100000);
    cb_fit.optimize();
    //cb_fit.print_parameters();

    trials = ad["sim"];
    result = Result{cb_fit.get_min_f(), cb_fit.get_count(), cb_fit.get_parameters()};
}


void run_simulations(int rep, int setting) {
    std::string s = std::to_string(setting);
    Results results, results_cb;
    AllData alldata, alldata_cb;

#ifdef ENABLE_OPENMP
        omp_lock_t writelock;
        omp_init_lock(&writelock);
#endif

#ifdef ENABLE_OPENMP
#pragma omp parallel for schedule (dynamic)
#endif
    for (int i = 0; i < rep; ++i) {
        std::string name = "rep" + std::to_string(i);
        std::cout << name << std::endl;
#ifdef ENABLE_OPENMP
        omp_set_lock(&writelock);
#endif
        results[name] = {};
        alldata[name] = {};
#ifdef ENABLE_OPENMP
        omp_unset_lock(&writelock);
#endif
        run_simulation(alldata[name], results[name], lams[setting], gams[setting], A);
    }
    save<Results>(results, "simulation_fit_"+s+".dat");
    save<AllData>(alldata, "simulation_data_"+s+".dat");

#ifdef ENABLE_OPENMP
#pragma omp parallel for schedule (dynamic)
#endif
    for (int i = 0; i < rep; ++i) {
        std::string name = "rep" + std::to_string(i);
        std::cout << name << std::endl;
#ifdef ENABLE_OPENMP
        omp_set_lock(&writelock);
#endif
        results_cb[name] = {};
        alldata_cb[name] = {};
#ifdef ENABLE_OPENMP
        omp_unset_lock(&writelock);
#endif
        run_simulation_cb(alldata_cb[name], results_cb[name], rhos[setting], ps[setting]);
    }
    save<Results>(results_cb, "simulation_cb_fit_"+s+".dat");
    save<AllData>(alldata_cb, "simulation_cb_data_"+s+".dat");
}

void run_simulations_decode(std::string s) {
    Results results;
    AllData alldata;
    load<Results>(results, "simulation_fit_"+s+".dat");
    load<AllData>(alldata, "simulation_data_"+s+".dat");

    std::ofstream of("simulation_decode_"+s+".txt");
    for (auto& result : results) {
        std::cout << "#" << result.first << std::endl;
        AllData ad = {{"sim", alldata[result.first]}};
        SumStat ss = {};
        std::map<std::string, int> nn = {{"sim", 10}};
        HHMM hhmm("sim", &ad, &ss, &nn,
                mass::RK_UNB, 10, mass::PK_GAUSS_INT_MAP, 3);
        hhmm.set_cue(0);
        hhmm.set_up(
                {},
                g_phase_sim,
                g_conditions_1,
                result.second.params);
        hhmm.decode_attention(&of);
    }

    load<Results>(results, "simulation_cb_fit_"+s+".dat");
    load<AllData>(alldata, "simulation_cb_data_"+s+".dat");

    std::ofstream ofcb("simulation_cb_decode_"+s+".txt");
    for (auto& result : results) {
        std::cout << "#" << result.first << std::endl;
        AllData ad = {{"sim", alldata[result.first]}};
        SumStat ss = {};
        std::map<std::string, int> nn = {{"sim", 10}};
        CB cb("sim", &ad, &ss, &nn,
                mass::RK_UNB, 10, mass::PK_GAUSS_INT_MAP, 3);
        cb.set_cue(0);
        cb.set_up(
                {},
                g_phase_sim2,
                g_conditions_1,
                result.second.params);
        cb.decode_attention(&ofcb);
    }

}


void export_data() {
    NeuralData nd;
    nd.set_phase(g_phase_5);
    nd.set_cue(0);
    nd.set_spike_num_thres(2);
    nd.read_data();

    save<AllData>(nd.get_data(), "MJMM.dat");
    print_data("MJMM.dat", "MJMM.txt");
}


void select_and_decode_hmm(const std::vector<double>& phase, const std::vector<std::vector<Obj>>& conditions) {
    Results results;

    NeuralData nd;
    nd.set_phase(phase);
    nd.set_cue(0);
    nd.set_spike_num_thres(2);
    nd.read_data();

    std::map<std::string, std::vector<std::string>> file_map = nd.get_file_map(nd.get_file_list());
    std::map<std::string, int> neuron_number = *(nd.get_neuron_number());

    std::vector<std::string> keys;
    for (auto& nn : neuron_number) {
        keys.push_back(nn.first);
    }

    for (size_t kid = 0; kid < keys.size(); ++kid) {
        auto& nn_first = keys[kid];
        auto& nn_second = neuron_number[nn_first];
        if (nn_second >= 5) {
            double minf = HUGE_VAL;
            std::string fname = "buildmktest/mkhmm_3_12_trial_1/mk_hmm_3_12_"
                + nn_first + "_rep_";
            for (int repi = 0; repi < 10; ++repi) {
                Result res;
                std::string fullname = fname + std::to_string(repi) + ".dat";
                std::ifstream infile(fullname);
                if (!infile.good()) continue;
                load<Result>(res, fullname);
                if (minf > res.min_f) {
                    minf = res.min_f;
                    results[nn_first] = res;
                }
            }
        }
    }

    save<Results>(results, "mktesttemp.dat");
    loadprint("mktesttemp.dat", "mktesttemp.txt");

    decode_hmm(phase, conditions, "mktesttemp.dat");
}


int main() {

    //simulations
    run_simulations(100, 0);
    run_simulations(100, 1);
    run_simulations(100, 2);
    print_data("simulation_data_0.dat", "simulation_data_0.txt");
    print_data("simulation_data_1.dat", "simulation_data_1.txt");
    print_data("simulation_data_2.dat", "simulation_data_2.txt");
    print_data("simulation_cb_data_0.dat", "simulation_cb_data_0.txt");
    print_data("simulation_cb_data_1.dat", "simulation_cb_data_1.txt");
    print_data("simulation_cb_data_2.dat", "simulation_cb_data_2.txt");
    loadprint("simulation_fit_0.dat", "simulation_fit_0.txt");
    loadprint("simulation_fit_1.dat", "simulation_fit_1.txt");
    loadprint("simulation_fit_2.dat", "simulation_fit_2.txt");
    loadprint("simulation_cb_fit_0.dat", "simulation_cb_fit_0.txt");
    loadprint("simulation_cb_fit_1.dat", "simulation_cb_fit_1.txt");
    loadprint("simulation_cb_fit_2.dat", "simulation_cb_fit_2.txt");
    run_simulations_decode("0");
    run_simulations_decode("1");
    run_simulations_decode("2");
    

    // fit hmm and cb and decode
    fit_hmm(g_phase_3, g_conditions_12, "mk_hmm_3_12");
    decode_hmm(g_phase_3, g_conditions_12, "mk_hmm_3_12.dat.optimal");
    fit_cb(g_phase_3, g_conditions_12, "mk_cb_3_12");
    decode_cb(g_phase_3, g_conditions_12, "mk_cb_3_12.dat.optimal");
    
    fit_hmm(g_phase_3, g_conditions_3, "mk_hmm_3_3");
    decode_hmm(g_phase_3, g_conditions_3, "mk_hmm_3_3.dat.optimal");
    fit_cb(g_phase_3, g_conditions_3, "mk_cb_3_3");
    decode_cb(g_phase_3, g_conditions_3, "mk_cb_3_3.dat.optimal");
    
    fit_hmm(g_phase_5, g_conditions_12, "mk_hmm_5_12");
    decode_hmm(g_phase_5, g_conditions_12, "mk_hmm_5_12.dat.optimal");
    fit_cb(g_phase_5, g_conditions_12, "mk_cb_5_12");
    decode_cb(g_phase_5, g_conditions_12, "mk_cb_5_12.dat.optimal");
    
    fit_hmm(g_phase_5, g_conditions_3, "mk_hmm_5_3");
    decode_hmm(g_phase_5, g_conditions_3, "mk_hmm_5_3.dat.optimal");
    fit_cb(g_phase_5, g_conditions_3, "mk_cb_5_3");
    decode_cb(g_phase_5, g_conditions_3, "mk_hmm_5_3.dat.optimal");
    
    fit_hmm(g_phase_10, g_conditions_12, "mk_hmm_10_12");
    decode_hmm(g_phase_10, g_conditions_12, "mk_hmm_10_12.dat.optimal");
    fit_hmm(g_phase_10, g_conditions_3, "mk_hmm_10_3");
    decode_hmm(g_phase_10, g_conditions_3, "mk_hmm_10_3.dat.optimal");


    // multiple initials
    fit_hmm(g_phase_3, g_conditions_3, "mk_hmm_lambda_3_3", false);

}
