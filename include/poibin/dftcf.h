#ifndef POIBIN_DFTCF_H
#define POIBIN_DFTCF_H

#include <vector>
#include <iostream>

namespace poibin {

std::vector<double> dpoibin(std::vector<int>& kk, std::vector<double>& pp);

std::vector<double> dpoibin(std::vector<double>& pp);

/*
int main() {

    std::vector<int> kk = {0, 1, 2, 3};
    std::vector<double> pp = {0.2, 0.3, 0.2, 0.1};

    std::vector<double> val = dpoibin(kk, pp);

    for (auto k : val)
        std::cout << k << "\n";

}
*/

} // namespace poibin


#endif
