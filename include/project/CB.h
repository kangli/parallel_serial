/*
 * The correlated Binomial model.
 *
 */

#ifndef PROJECT_CB_H
#define PROJECT_CB_H

#include <project/Utils.h>
#include <project/NeuralData.h>
#include <mass/CIF.h>
#include <mass/Helper.h>

#include <cmath>

namespace project {


// hierarchical hidden markov model
class CB : public mass::CIF<ObjStim> {

public:

    CB(std::string name,  mass::FuncType ft_rk, int n_rk, 
            mass::FuncType ft_pk, int n_pk)
    : CIF(name, ft_rk, n_rk, ft_pk, n_pk) {
#if APPLE==1
        m_folder = "/Users/";
#else
        m_folder = "/home/";
#endif
    }

    CB(std::string name, AllData* p_ad, SumStat* p_ss,
            std::map<std::string, int>* p_m, mass::FuncType ft_rk, int n_rk, 
            mass::FuncType ft_pk, int n_pk)
    : CIF(name, ft_rk, n_rk, ft_pk, n_pk) {
        set_parameter_names();
        mp_all_data = p_ad;
        mp_sum_stat = p_ss;
        set_keys();
        m_current_neuron_number = (*p_m)[name];
#if APPLE==1
        m_folder = "/Users/";
#else
        m_folder = "/home/";
#endif
    }

    void set_up(
            const std::vector<std::string>& files,
            const std::vector<double>& phase,
            const std::vector<std::vector<Obj>>& conditions,
            const std::vector<double> params
            ) {
        m_phase = phase;
        m_conditions = conditions;

        // relabel conditions
        relabel_conditions(*mp_all_data, m_conditions);
        //print_conditions(*mp_all_data);

        // initialize m_rates based on neuron number
        m_rates.resize(m_current_neuron_number*7, 0);

        // initialize rho and p
        int phase_num = phase.size() / 2;
        int cond_num = conditions.size();
        m_rho.resize(cond_num * (phase_num-1) + 1);
        m_p.resize(cond_num * (phase_num-1) + 1);

        set_parameters(params);
    }

    void set_up(
            const std::vector<std::string>& files,
            const std::vector<double>& phase,
            const std::vector<std::vector<Obj>>& conditions,
            const std::vector<double>& rho,
            const std::vector<double>& rho_lb,
            const std::vector<double>& rho_ub,
            const std::vector<double>& p,
            const std::vector<double>& p_lb,
            const std::vector<double>& p_ub,
            const std::vector<double>& rates,
            const std::vector<double>& rates_lb,
            const std::vector<double>& rates_ub,
            double adapt, double adapt_lb, double adapt_ub,
            const std::vector<double>& cif,
            const std::vector<double>& cif_lb,
            const std::vector<double>& cif_ub
            ) {
        m_phase = phase;
        m_conditions = conditions;

        // relabel conditions
        relabel_conditions(*mp_all_data, m_conditions);
        //print_conditions(*mp_all_data);

        // initialize m_rates based on neuron number
        m_rates.resize(m_current_neuron_number*7, 0);

        // initialize rho and p
        int phase_num = phase.size() / 2;
        int cond_num = conditions.size();
        m_rho.resize(cond_num * (phase_num-1) + 1);
        m_p.resize(cond_num * (phase_num-1) + 1);

        // set initial parameters
        std::vector<double> params, lb, ub;
        // same rho for all cond
        /*
        std::vector<double> rholb(21, 0.5);
        std::vector<double> rhoub(21, 0.5);
        rholb[0] = 0.001;
        rholb[1] = 0.001;
        rholb[2] = 0.001;
        rhoub[0] = 0.999;
        rhoub[1] = 0.999;
        rhoub[2] = 0.999;
        */
        // rho
        cont_vectors(params, rho);
        cont_vectors(lb, rho_lb);
        cont_vectors(ub, rho_ub);
        // p
        cont_vectors(params, p);
        cont_vectors(lb, p_lb);
        cont_vectors(ub, p_ub);
        // CIF
        // rate
        std::vector<double> rate, rate_lb, rate_ub;
        get_rate_params_from_data(files, rate, rate_lb, rate_ub);
        cont_vectors(params, rate);
        cont_vectors(lb, rate_lb);
        cont_vectors(ub, rate_ub);
        params.push_back(adapt);
        lb.push_back(adapt_lb);
        ub.push_back(adapt_ub);
        cont_vectors(params, cif);
        cont_vectors(lb, cif_lb);
        cont_vectors(ub, cif_ub);

        // set parameters and bounds
        set_parameters(params);
        set_lower_bounds(lb);
        set_upper_bounds(ub);
    }

    virtual double calc_obj_func() const {
        double r = 0;
        for (auto& keys : m_keys) {
            for (size_t i = 0; i < keys.second.size(); ++i) {
                ObjStim objstim;
                auto& tmpmd = (*mp_all_data)[keys.first][keys.second[i]][0];
                if (m_cue != 0 && tmpmd.cue != m_cue) continue;
                if (tmpmd.condition > 16) continue;
                objstim.obj_cont = tmpmd.contra;
                objstim.obj_ipsi = tmpmd.ipsi;
                int phase_num = m_phase.size() / 2;
                for (int j = 0; j < phase_num; j++) {
                    double pbin = 1, pber = 1, pber1 = 1, pber2 = 1;
                    double p = 0, rho = 0;
                    get_p_rho(j, tmpmd, rho, p);
                    for (auto& md : (*mp_all_data)[keys.first][keys.second[i]]) {
                        objstim.hemisphere = 0;
                        objstim.neuron = md.index;
                        objstim.phase = j;
                        //double pps1 = exp(calc_spike_train_loglik(
                        //        objstim, md.spike, m_phase[j*2], m_phase[j*2+1]));
                        double pps1 = calc_spike_train_loglik(
                                objstim, md.spike, m_phase[j*2], m_phase[j*2+1]);
                        objstim.hemisphere = 1;
                        //double pps2 = exp(calc_spike_train_loglik(
                        //        objstim, md.spike, m_phase[j*2], m_phase[j*2+1]));
                        double pps2 = calc_spike_train_loglik(
                                objstim, md.spike, m_phase[j*2], m_phase[j*2+1]);
                        //pbin *= p*pps1 + (1-p)*pps2;
                        //pber1 *= pps1; pber2 *= pps2;
                        std::vector<double> logs = {log(p) + pps1, log(1-p) + pps2};
                        pbin += mass::log_sum_exp(logs);
                        pber1 += pps1; pber2 += pps2;
                    }
                    std::vector<double> logs2 = {log(p)+pber1, log(1-p)+pber2};
                    double log2 = mass::log_sum_exp(logs2);
                    logs2 = {log(1-rho) + pbin, log(rho)+log2};
                    double ans = mass::log_sum_exp(logs2);
                    //double ans = log((1-rho)*pbin + rho*(p*pber1 + (1-p)*pber2));
                    if (ans < -1e10 || ans > 1e300) std::cout << ans << "\t" << keys.first << "\t" << tmpmd.name << "\t" << j << std::endl;
                    if (ans < -1e10) ans = -1e10;
                    else if (ans > 1e300) ans = 1e300;
                    r += ans;
                }
            }
        }
        return - (r + calc_prior());
    }

    void simulate_spike_train(const ObjStim& stim, std::vector<double>& spike_train, double start, double end) const {
        std::mt19937 gen(rand());
        std::uniform_real_distribution<> dis(0, 1);
        // including start and end time points
        int size = std::round((end-start)/0.001);
        for (int i = 0; i <= size; ++i) {
            double prob = calc_cif(stim, spike_train, start+i*0.001) * 0.001;
            if (dis(gen) < prob) spike_train.push_back(start+i*0.001);
        }
    }

    AllData simulate_data(
            std::vector<double> rho,
            std::vector<double> p,
            std::vector<double> rates,
            double adapt,
            std::vector<double> weights,
            std::vector<double> phases,
            std::vector<std::vector<Obj>> conditions,
            int phasenum,
            int trialnum, int neuronnum
            ) {
        mp_rk->set_parameters(0, weights);
        Trials trials;
        std::random_device rd;
        std::mt19937 gen(rd());
        ObjStim stim;
        stim.obj_cont = OBJ_T;
        stim.obj_ipsi = OBJ_NI;

        // assign rates
        m_rates.resize(7*neuronnum);
        auto it = rates.begin();
        for (int j = 0; j < neuronnum; ++j) {
            for (int i = 0; i < 7; ++i) {
                m_rates[j*7+i] = *it;
                ++it;
            }
        }
        m_adapt = adapt;


        for (int i = 0; i < trialnum; ++i) {
            trials[i] = std::vector<DataMK>(neuronnum);
            for (int j = 0; j < neuronnum; ++j) {
                trials[i][j].name = std::string("neuron") + std::to_string(j);
                trials[i][j].index = j;
                trials[i][j].condition = 0;
                trials[i][j].cue = 0;
                trials[i][j].contra = OBJ_T;
                trials[i][j].ipsi = OBJ_NI;
                trials[i][j].spike = {};
                trials[i][j].rate = {};
            }

            auto it_rho = rho.begin();
            auto it_p = p.begin();
            for (int t = 0 ; t < phasenum; ++t) {
                stim.phase = t;
                std::discrete_distribution<int> disc_c{*it_rho, 1-*it_rho};
                std::discrete_distribution<int> disc_o{*it_p, 1-*it_p};
                int c = disc_c(gen);
                if (c == 1) {
                    for (int j = 0; j < neuronnum; ++j) {
                        int o = disc_o(gen);
                        stim.hemisphere = o;
                        stim.neuron = j;
                        simulate_spike_train(stim, trials[i][j].spike, phases[2*t], phases[2*t+1]);
                    }
                } else {
                    int o = disc_o(gen);
                    stim.hemisphere = o;
                    for (int j = 0; j < neuronnum; ++j) {
                        stim.neuron = j;
                        simulate_spike_train(stim, trials[i][j].spike, phases[2*t], phases[2*t+1]);
                    }
                }
                it_rho++; it_p++;
            }

        }
        AllData alldata = {{"sim", trials}};
        return alldata;
    }

    // structure of llk:
    // { { {h=0}, {h=1} }, { {h=0}, {h=1} }, ... , }
    //   ------- t=0 ---    -----t=1-------
    void decode_attention(std::ofstream* ofp = NULL) {
        // Decode
        // { decode_t=1, decode_t=2,... }
        std::map<int, std::vector<Decode>> decs;
        auto keys = *(m_keys.begin());
        for (size_t i = 0; i < keys.second.size(); ++i) {
            ObjStim objstim;
            auto& tmpmd = (*mp_all_data)[keys.first][keys.second[i]][0];
            if (m_cue != 0 && tmpmd.cue != m_cue) continue;
            if (tmpmd.condition > 16) continue;
            objstim.obj_cont = tmpmd.contra;
            objstim.obj_ipsi = tmpmd.ipsi;
            int phase_num = m_phase.size() / 2;
            int n = (*mp_all_data)[keys.first][keys.second[i]].size();
            std::vector<Decode> dec(phase_num);
            for (int j = 0; j < phase_num; j++) {
                double pbin = 1, pber = 1, pber1 = 1, pber2 = 1;
                double p = 0, rho = 0;
                get_p_rho(j, tmpmd, rho, p);
                for (auto& md : (*mp_all_data)[keys.first][keys.second[i]]) {
                    objstim.hemisphere = 0;
                    objstim.neuron = md.index;
                    objstim.phase = j;
                    double pps1 = exp(calc_spike_train_loglik(
                                objstim, md.spike, m_phase[j*2], m_phase[j*2+1]));
                    objstim.hemisphere = 1;
                    double pps2 = exp(calc_spike_train_loglik(
                                objstim, md.spike, m_phase[j*2], m_phase[j*2+1]));
                    // PX_C1
                    dec[j].px_c1.push_back(p*pps1 / (p*pps1 + (1-p)*pps2));

                    pbin *= p*pps1 + (1-p)*pps2;
                    pber1 *= pps1; pber2 *= pps2;
                }
                // PC
                dec[j].pc = (1-rho)*pbin / ((1-rho)*pbin + rho*(p*pber1 + (1-p)*pber2));  // prob of being Bin
                // PX_C2
                dec[j].px_c2 = std::vector<double>(n, p*pber1 / (p*pber1 + (1-p)*pber2));
                dec[j].num = n;

                //PMF PX
                double w1 = dec[j].pc;
                double w2 = 1 - w1;
                auto pmf1 = poibin::dpoibin(dec[j].px_c1);
                for (int l = 0; l <= n; ++l) {
                    if (l != n) {
                        double px = dec[j].px_c1[l] * w1 + dec[j].px_c2[l] * w2;
                        dec[j].px.push_back(px);
                    }
                    double tmp = pmf1[l] * w1;
                    if (l == 0) tmp += (1-dec[j].px_c2[0]) * w2;
                    if (l == n) tmp += (dec[j].px_c2[0]) * w2;
                    dec[j].pmf.push_back(tmp);
                }

                // calc Dn
                double Dn = 0;
                for (int l = 0; l <= n; ++l) {
                    Dn += abs(l - 0.5*n) * dec[j].pmf[l];
                }
                dec[j].Dn = Dn / 0.5 / n;
            }
            decs[keys.second[i]] = dec;
        }
        // output decoding
        if (ofp == NULL) {
            for (auto& d : decs) {
                int j = 0;
                for (auto& dd : d.second) {
                    std::cout << mp_all_data->begin()->first << "\t" << d.first << "\t" << mp_all_data->begin()->second[d.first][0].condition << "\t" << j << "\t" << m_cue << "\t" << dd.num << "\t" << dd.Dn << "\t" << dd.pc << "\t";
                    for (double p : dd.pmf) std::cout << p << "\t";
                    for (double p : dd.px) std::cout << p << "\t";
                    std::cout << "\n";
                    ++j;
                }
            }
        } else {
            for (auto& d : decs) {
                int j = 0;
                for (auto& dd : d.second) {
                    *ofp << mp_all_data->begin()->first << "\t" << d.first << "\t" << mp_all_data->begin()->second[d.first][0].condition << "\t" << j << "\t" << m_cue << "\t" << dd.num << "\t" << dd.Dn << "\t" << dd.pc << "\t" << dd.pc1 << "\t" << dd.pc2 << "\t";
                    for (double p : dd.pmf) *ofp << p << "\t";
                    for (double p : dd.px) *ofp << p << "\t";
                    *ofp << "\n";
                    ++j;
                }
            }
        }
    }


    virtual double calc_obj_func_decode() const {
        return 0;
    }

    virtual double calc_cif(const ObjStim& stim, const std::vector<double>& ss, 
            double t) const {
        double ada = t * m_adapt;
        int m = 0;
        for (auto i : ss) {
            if (i < t) m = i;
            else break;
        }
        double h = mp_rk->calc(m, t, ss);
        return calc_rate(stim, t) * exp(h + ada);
    }

    void get_rate_params_from_data(
            const std::vector<std::string>& files,
            std::vector<double>& rate,
            std::vector<double>& lb,
            std::vector<double>& ub ) const {
        std::string file_pre = "data/Data_set_MJ_MM/";
        std::string file_pos = ".mat";
        double rr = 1.0,
               rl = 0.2,
               ru = 5.0;
        for(size_t i = 0; i < files.size(); ++i) {
            std::vector<double> tmprs;
            std::string file = file_pre + files[i] + file_pos;
            tmprs.push_back(calc_mean((*mp_sum_stat)[file].t_cont_rate));
            tmprs.push_back(calc_mean((*mp_sum_stat)[file].ni_cont_rate));
            tmprs.push_back(calc_mean((*mp_sum_stat)[file].nc_cont_rate));
            tmprs.push_back(calc_mean((*mp_sum_stat)[file].t_ipsi_rate));
            tmprs.push_back(calc_mean((*mp_sum_stat)[file].ni_ipsi_rate));
            tmprs.push_back(calc_mean((*mp_sum_stat)[file].nc_ipsi_rate));
            for (double tmpr : tmprs) {
                if (std::isfinite(tmpr)) {
                    if (tmpr < 1) tmpr = 1;
                    rate.push_back(tmpr*rr);
                    lb.push_back(tmpr*rl);
                    ub.push_back(tmpr*ru);
                } else {
                    rate.push_back(10);
                    lb.push_back(1);
                    ub.push_back(60);
                }
            }
            double tmpr = (*mp_sum_stat)[file].base_rate;
            rate.push_back(tmpr);
            lb.push_back(tmpr*0.2);
            ub.push_back(tmpr*5.0);
        }
    }

    void randomize_params_mul(std::vector<double>& params, double l, double h) {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<> dis(l, h);
        for (auto& v : params) {
            v *= dis(gen);
        }
    }

    void randomize_params_set(std::vector<double>& params, double l, double h) {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<> dis(l, h);
        for (auto& v : params) {
            v = dis(gen);
        }
    }


    virtual void set_parameter_names() {
    }

    void print_parameters() {
        mass::CIF<ObjStim>::print_parameters();
    }

    void set_cue(int cue) { m_cue = cue; }


protected:

    void set_keys() {
        m_keys.clear();
        for (auto& trials : (*mp_all_data)) {
            m_keys[trials.first] = {};
            for (auto& mdv : trials.second) {
                m_keys[trials.first].push_back(mdv.first);
            }
        }
    }

    // prior for rho: gaussian
    double calc_prior() const {
        double r = 0;
        for (double rho : m_rho) {
            r += (rho - 0.5) * (rho - 0.5);
        }
        return 0;
    }

    double calc_mean(const std::vector<double>& v) const {
        double r = 0;
        for (double i : v) {
            r += i;
        }
        return r*1.0/v.size();
    }

    // parameters:
    virtual int process_parameters() {
        int index = 0;
        // rho
        for (size_t i = 0; i < m_rho.size(); ++i) {
            m_rho[i] = m_params[index+i];
        }
        index += m_rho.size();

        // p
        for (size_t i = 0; i < m_p.size(); ++i) {
            m_p[i] = m_params[index+i];
        }
        index += m_p.size();

        // rates
        for (int j = 0; j < m_current_neuron_number; ++j) {
            for (int i = 0; i < 7; ++i) {
                m_rates[j*7+i] = m_params[index];
                ++index;
            }
        }

        // adapt
        m_adapt = m_params[index]; ++index;

        // response kernel
        return mp_rk->set_parameters(index, m_params);
    }

    // calculate the rate value for point process model
    virtual double calc_rate(const ObjStim& stim, double t) const {
        double rate;
        int i = stim.neuron * 7;
        Obj obj;
        int h = stim.hemisphere;
        if (h == 0) {
            obj = stim.obj_cont;
        } else {
            obj = stim.obj_ipsi;
        }
        switch (obj) {
            case OBJ_T:
                rate = h==0 ? m_rates[0+i] : m_rates[3+i];
                break;
            case OBJ_NI:
                rate = h==0 ? m_rates[1+i] : m_rates[4+i];
                break;
            case OBJ_NC:
                rate = h==0 ? m_rates[2+i] : m_rates[5+i];
                break;
            case OBJ_NONE:
                rate = m_rates[6+i];
                break;
            default:
                rate = m_rates[6+i];
        }
        /*
        switch (obj) {
            case OBJ_T:
                rate = m_rates[0+i];
                break;
            case OBJ_NI:
                rate = m_rates[1+i];
                break;
            case OBJ_NC:
                rate = m_rates[2+i];
                break;
            case OBJ_NONE:
                rate = m_rates[6+i];
                break;
            default:
                rate = m_rates[6+i];
        }
        */
        return rate;
    }

    void get_p_rho(int time, DataMK& dm, double& rho, double& p) const {
        /*
        int cond = dm.condition;
        if (dm.cue == 2) cond = cond - 8;
        if (time == 0) {
            rho = m_rho[0];
            p = m_p[0];
        } else {
            if (cond == 7) cond = 9;
            else if (cond == 8) cond = 10;
            else if (cond == 5 && dm.ipsi == OBJ_NC) cond = 6;
            else if (cond == 6 && dm.contra == OBJ_NI) cond = 7;
            else if (cond == 6 && dm.contra == OBJ_NC) cond = 8;
            int i = (cond-1)%10;
            i = 10 * (time-1) + i + 1;
            rho = m_rho[i];
            p = m_p[i];
            //rho = m_rho[time];
        }
        */
        int cond = dm.condition;
        if (time == 0) {
            rho = m_rho[0];
            p = m_p[0];
        } else {
            int i = m_conditions.size() * (time-1) + cond + 1;
            rho = m_rho[i];
            p = m_p[i];
        }
    }

    void cont_vectors(std::vector<double>& v,
            const std::vector<double>& vv) {
        for (auto& j : vv) {
            v.push_back(j);
        }
    }

    // data
    AllData* mp_all_data;
    SumStat* mp_sum_stat;
    int m_current_neuron_number;
    std::map<std::string, std::vector<int>> m_keys; // filename -> {trialnum}

    // setup
    std::vector<double> m_phase;
    int m_cue = 1;
    std::vector<std::vector<Obj>> m_conditions;
    std::string m_folder;

    // parameters
    std::vector<double> m_rho;
    std::vector<double> m_p;
    std::vector<double> m_rates;
    double m_adapt;

}; // class CB

} // namespace project

#endif // CB_H
