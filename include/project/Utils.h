#ifndef PROJECT_UTILS_H
#define PROJECT_UTILS_H

namespace project {

struct Decode {
    double pc;
    double pc1;
    double pc2;
    std::vector<double> px_c1;
    std::vector<double> px_c2;
    std::vector<double> px_c3;
    std::vector<double> pmf;
    std::vector<double> px;
    double weight;
    double Dn;
    int num;
};

void relabel_conditions(AllData& ad, const std::vector<std::vector<Obj>>& conditions) {
    for (auto& session : ad) {
        for (auto& trials : session.second) {
            auto& md = trials.second[0];
            int cond = 0;
            for (size_t i = 0; i < conditions.size(); ++i) {
                for (size_t j = 0; j < conditions[i].size() / 2; ++j) {
                    if (md.contra == conditions[i][2*j] && md.ipsi == conditions[i][2*j+1]) {
                        cond = i;
                        break;
                    }
                }
            }
            for (auto& md : trials.second) {
                md.condition = cond;
            }
        }
    }
}

void print_conditions(AllData& ad) {
    for (auto& session : ad) {
        for (auto& trials : session.second) {
            auto& md = trials.second[0];
            std::cout << md.condition << "\t" << md.contra << "\t" << md.ipsi << std::endl;
        }
    }
}

} // namespace project

#endif // PROJ)XMK_UTILS_H
