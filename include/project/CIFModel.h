#ifndef PROJECT_CIFMODEL_H
#define PROJECT_CIFMODEL_H

#include <project/NeuralData.h>
#include <mass/CIF.h>

#include <cmath>

namespace project {

class CIFModel : public mass::CIF<ObjStim> {

public:

    CIFModel(std::string name, AllData* p_ad, SumStat* p_ss,
             std::map<std::string, int>* p_m, mass::FuncType ft_rk, int n_rk, 
             mass::FuncType ft_pk, int n_pk)
        : CIF(name, ft_rk, n_rk, ft_pk, n_pk) {
        set_parameter_names();
        mp_all_data = p_ad;
        mp_sum_stat = p_ss;
        set_keys();
        m_current_neuron_number = (*p_m)[name];
    }

    double calc_obj_func_serial() const {
        double r = 0;
        for (auto& keys : m_keys) {
#ifdef ENABLE_OPENMP
#pragma omp parallel for schedule (dynamic)
#endif
            for (size_t i = 0; i < keys.second.size(); ++i) {
                ObjStim objstim;
                auto& tmpmd = (*mp_all_data)[keys.first][keys.second[i]][0];
                objstim.obj_cont = tmpmd.contra;
                objstim.obj_ipsi = tmpmd.ipsi;
                double L11 = 0, L12 = 0, L21 = 0, L22 = 0, L31 = 0, L32 = 0;
                for (auto& md : (*mp_all_data)[keys.first][keys.second[i]]) {
                    if ((m_empirical_likelihood || !m_infer_rate ) &&
                            (md.contra == OBJ_NONE || md.ipsi == OBJ_NONE))
                        continue;
                    objstim.neuron = md.index;
                    objstim.name = md.name;
                    L11 += calc_loglik_from_condition(
                            objstim, md.spike, m_phase[0], m_phase[1], 0, 0);
                    L12 += calc_loglik_from_condition(
                            objstim, md.spike, m_phase[0], m_phase[1], 0, 1);
                    L21 += calc_loglik_from_condition(
                            objstim, md.spike, m_phase[2], m_phase[3], 1, 0);
                    L22 += calc_loglik_from_condition(
                            objstim, md.spike, m_phase[2], m_phase[3], 1, 1);
                    L31 += calc_loglik_from_condition(
                            objstim, md.spike, m_phase[4], m_phase[5], 2, 0);
                    L32 += calc_loglik_from_condition(
                            objstim, md.spike, m_phase[4], m_phase[5], 2, 1);
                }
                // log like
                int probi = calc_prob_index(objstim);
                const std::vector<double>& probs = m_p[probi];
                const std::vector<double>& probs2 = m_changing_tpm ? m_p2[probi]
                                                                   : m_p[probi];
                std::vector<double> logs;
                logs.push_back(log(probs[0]) + log(probs[1]) + log(probs2[1]) +
                        L11 + L21 + L31);
                logs.push_back(log(probs[0]) + log(1-probs[1]) + log(probs2[2]) +
                        L11 + L22 + L31);
                logs.push_back(log(1-probs[0]) + log(probs[2]) + log(probs2[1]) +
                        L12 + L21 + L31);
                logs.push_back(log(1-probs[0]) + log(1-probs[2]) + log(probs2[2]) +
                        L12 + L22 + L31);
                logs.push_back(log(probs[0]) + log(probs[1]) + log(1-probs2[1]) +
                        L11 + L21 + L32);
                logs.push_back(log(probs[0]) + log(1-probs[1]) + log(1-probs2[2]) +
                        L11 + L22 + L32);
                logs.push_back(log(1-probs[0]) + log(probs[2]) + log(1-probs2[1]) +
                        L12 + L21 + L32);
                logs.push_back(log(1-probs[0]) + log(1-probs[2]) + log(1-probs2[2]) +
                        L12 + L22 + L32);
                double tmp = log_sum_exp(logs);
#ifdef ENABLE_OPENMP
#pragma omp atomic
#endif
                r += tmp;
            }
        }
        return -r;
    }

    double calc_obj_func_parallel() const {
        double r = 0;
        for (auto& keys : m_keys) {
#ifdef ENABLE_OPENMP
#pragma omp parallel for schedule (dynamic)
#endif
            for (size_t i = 0; i < keys.second.size(); ++i) {
                ObjStim objstim;
                for (auto& md : (*mp_all_data)[keys.first][keys.second[i]]) {
                    if ((m_empirical_likelihood || !m_infer_rate ) &&
                            (md.contra == OBJ_NONE || md.ipsi == OBJ_NONE))
                        continue;
                    objstim.neuron = md.index;
                    objstim.name = md.name;
                    objstim.obj_cont = md.contra;
                    objstim.obj_ipsi = md.ipsi;
                    // early cont
                    double L11 = calc_loglik_from_condition(objstim, md.spike, 
                            m_phase[0], m_phase[1], 0, 0);
                    // early ipsi
                    double L12 = calc_loglik_from_condition(objstim, md.spike, 
                            m_phase[0], m_phase[1], 0, 1);
                    // middle cont
                    double L21 = calc_loglik_from_condition(objstim, md.spike, 
                            m_phase[2], m_phase[3], 1, 0);
                    // middle ipsi
                    double L22 = calc_loglik_from_condition(objstim, md.spike, 
                            m_phase[2], m_phase[3], 1, 1);
                    // late cont
                    double L31 = calc_loglik_from_condition(objstim, md.spike, 
                            m_phase[4], m_phase[5], 2, 0);
                    // late ipsi
                    double L32 = calc_loglik_from_condition(objstim, md.spike, 
                            m_phase[4], m_phase[5], 2, 1);
                    // log like
                    int probi = calc_prob_index(objstim);
                    const std::vector<double>& probs = m_p[probi];
                    const std::vector<double>& probs2 = 
                        m_changing_tpm ? m_p2[probi] : m_p[probi];
                    std::vector<double> logs;
                    logs.push_back(log(probs[0]) + log(probs[1]) + log(probs2[1]) 
                            + L11 + L21 + L31);
                    logs.push_back(log(probs[0]) + log(1-probs[1]) + log(probs2[2]) 
                            + L11 + L22 + L31);
                    logs.push_back(log(1-probs[0]) + log(probs[2]) + log(probs2[1])
                            + L12 + L21 + L31);
                    logs.push_back(log(1-probs[0]) + log(1-probs[2]) + log(probs2[2])
                            + L12 + L22 + L31);
                    logs.push_back(log(probs[0]) + log(probs[1]) + log(1-probs2[1])
                            + L11 + L21 + L32);
                    logs.push_back(log(probs[0]) + log(1-probs[1]) + log(1-probs2[2])
                            + L11 + L22 + L32);
                    logs.push_back(log(1-probs[0]) + log(probs[2]) + log(1-probs2[1])
                            + L12 + L21 + L32);
                    logs.push_back(log(1-probs[0])+log(1-probs[2])+log(1-probs2[2])
                            + L12 + L22 + L32);
                    double tmp = log_sum_exp(logs);
#ifdef ENABLE_OPENMP
#pragma omp atomic
#endif
                    r += tmp;
                }
            }
        }
        //std::cout << r << std::endl;
        return -r;
    }

    double calc_obj_func_random_switch() const {
        double r = 0;
        for (auto& keys : m_keys) {
#ifdef ENABLE_OPENMP
#pragma omp parallel for schedule (dynamic)
#endif
            for (size_t i = 0; i < keys.second.size(); ++i) {
                ObjStim objstim;
                double tmp = 0;
                for (auto& md : (*mp_all_data)[keys.first][keys.second[i]]) {
                    if ((m_empirical_likelihood || !m_infer_rate ) &&
                            (md.contra == OBJ_NONE || md.ipsi == OBJ_NONE))
                        continue;
                    objstim.neuron = md.index;
                    objstim.name = md.name;
                    objstim.obj_cont = md.contra;
                    objstim.obj_ipsi = md.ipsi;
                    // if one is none, use full spike train
                    if (md.contra == OBJ_NONE) {
                        tmp += calc_loglik_from_condition(
                                objstim, md.spike, 0.05, 0.5, 0, 1);
                    } else if (md.ipsi == OBJ_NONE) {
                        tmp += calc_loglik_from_condition(
                                objstim, md.spike, 0.05, 0.5, 0, 0);
                    } else {
                        int neuron_i = m_switch_time_diff_neuron ? 
                            objstim.neuron : 0;
                        int stim_i = m_switch_time_diff_stimcond ? 
                            calc_prob_index(objstim) : 0;
                        double switch_time = m_switch_time[neuron_i][stim_i];
                        tmp += calc_loglik_from_condition(
                                objstim, md.spike, 0.05, switch_time, 0, 0);
                        tmp += calc_loglik_from_condition(
                                objstim, md.spike, switch_time, 0.5, 1, 1);
                    }
                }
#ifdef ENABLE_OPENMP
#pragma omp atomic
#endif
                r += tmp;
            }
        }
        return -r;
    }

    virtual double calc_obj_func() const {
        if (0 == m_method) {
            return calc_obj_func_parallel();
        } else if (1 == m_method) {
            return calc_obj_func_serial();
        } else {
            return calc_obj_func_random_switch();
        }
    }

    double calc_loglik_from_condition(
            ObjStim& objstim, const std::vector<double>& spike, 
            double begin, double end, int phase, int hemisphere) const {
        double value = 0;
        if (m_empirical_likelihood) {
            int c = 0;
            double s = m_phase[2*phase];
            double e = m_phase[2*phase+1];
            for (auto v : spike) {
                if (v >= s && v <= e) {
                    c++;
                } else if (v > e) {
                    break;
                }
            }
            double r = c / (e-s);
            auto mss = mp_sum_stat;
            if (hemisphere == 0) {
                switch (objstim.obj_cont) {
                    case OBJ_T:
                        value = (*mss)[objstim.name].t_cont_epdf[phase].calc(r);
                        break;
                    case OBJ_NI:
                        value = (*mss)[objstim.name].ni_cont_epdf[phase].calc(r);
                        break;
                    case OBJ_NC:
                        value = (*mss)[objstim.name].nc_cont_epdf[phase].calc(r);
                        break;
                    case OBJ_NONE:
                        std::cout << "debug" << std::endl;
                        break;
                }
            } else {
                switch (objstim.obj_ipsi) {
                    case OBJ_T:
                        value = (*mss)[objstim.name].t_ipsi_epdf[phase].calc(r);
                        break;
                    case OBJ_NI:
                        value = (*mss)[objstim.name].ni_ipsi_epdf[phase].calc(r);
                        break;
                    case OBJ_NC:
                        value = (*mss)[objstim.name].nc_ipsi_epdf[phase].calc(r);
                        break;
                    case OBJ_NONE:
                        std::cout << "debug" << std::endl;
                        break;
                }
            }
            if (value < 1e-300) value = 1e-300;
            value = log(value);
        } else {
            objstim.phase = phase;
            objstim.hemisphere = hemisphere;
            value = calc_spike_train_loglik(objstim, spike, begin, end);
        }
        // in case of nan or inf
        if (std::isinf(value) || std::isnan(value)) value = 0;
        return value;
    }

    /*
    int calc_prob_index(const ObjStim& stim) const {
        int i;
        if (stim.obj_cont == OBJ_T) {
            i = 0;
        } else if (stim.obj_ipsi == OBJ_T) {
            i = 1;
        } else {
            i = 2;
        }
        return i;
    }
    */

    int calc_prob_index(const ObjStim& stim) const {
        Obj cont = stim.obj_cont;
        Obj ipsi = stim.obj_ipsi;
        int r = 0;
        for (size_t i=0; i < m_prob_condition.size(); ++i) {
            if (cont == m_prob_condition[i][0] &&
                    ipsi == m_prob_condition[i][1]) {
                break;
                r = i;
            }
        }
        return m_prob_index[r];
    }
    /*
    int calc_prob_index(const ObjStim& stim) const {
        int i;
        if (stim.obj_cont == OBJ_NONE) {
            i = 0;
        } else if (stim.obj_ipsi == OBJ_T) {
            i = 1;
        } else {
            i = 2;
        }
        return i;
    }
    */

    virtual double calc_obj_func_decode() const {
        return 0;
    }
    
    virtual double calc_cif(const ObjStim& stim, const std::vector<double>& ss, 
            double t) const {
        int m = 0;
        for (auto i : ss) {
            if (i < t) m = i;
            else break;
        }
        double h = mp_rk->calc(m, t, ss);
        return calc_rate(stim, t) * exp(h);
    }

    virtual double calc_rate(const ObjStim& stim, double t) const {
        double rate;
        int i = stim.neuron * 4;
        if (m_infer_rate) {
            Obj obj;
            if (stim.hemisphere == 0) {
                obj = stim.obj_cont;
            } else {
                obj = stim.obj_ipsi;
            }
            switch (obj) {
                case OBJ_T:
                    rate = m_rates[0+i];
                    break;
                case OBJ_NI:
                    rate = m_rates[1+i];
                    break;
                case OBJ_NC:
                    rate = m_rates[2+i];
                    break;
                case OBJ_NONE:
                    rate = m_rates[3+i];
                    break;
                default:
                    rate = m_rates[3+i];
            }
        } else {
            if (stim.hemisphere == 0) {
                switch (stim.obj_cont) {
                    case OBJ_T:
                        rate = (*mp_sum_stat)[stim.name].t_cont_rate[stim.phase];
                        break;
                    case OBJ_NI:
                        rate = (*mp_sum_stat)[stim.name].ni_cont_rate[stim.phase];
                        break;
                    case OBJ_NC:
                        rate = (*mp_sum_stat)[stim.name].nc_cont_rate[stim.phase];
                        break;
                    case OBJ_NONE:
                        rate = m_rates[3+i];
                        break;
                    default:
                        rate = m_rates[3+i];
                }
            } else {
                switch (stim.obj_ipsi) {
                    case OBJ_T:
                        rate = (*mp_sum_stat)[stim.name].t_ipsi_rate[stim.phase];
                        break;
                    case OBJ_NI:
                        rate = (*mp_sum_stat)[stim.name].ni_ipsi_rate[stim.phase];
                        break;
                    case OBJ_NC:
                        rate = (*mp_sum_stat)[stim.name].nc_ipsi_rate[stim.phase];
                        break;
                    case OBJ_NONE:
                        rate = m_rates[3+i];
                        break;
                    default:
                        rate = m_rates[3+i];
                }
            }
        }
        return rate;
    }

    virtual void set_parameter_names() {
    }

    void set_phase(const std::vector<double>& phase) {
        m_phase = phase;
    }

    void set_up(const std::vector<double>& tpm,
            const std::vector<double>& tpm_u,
            const std::vector<double>& tpm_l,
            const std::vector<double>& rate,
            const std::vector<double>& rate_u,
            const std::vector<double>& rate_l,
            const std::vector<double>& cif,
            const std::vector<double>& cif_u,
            const std::vector<double>& cif_l,
            std::vector<std::vector<Obj>> prob_condition,
            std::vector<int> prob_index,
            bool infer_rate, bool emprical_likelihood) {
        m_st_neuron_num = m_switch_time_diff_neuron ? m_current_neuron_number : 1;
        m_st_stim_num = m_switch_time_diff_neuron ? prob_condition.size() : 1;
        m_rates.resize(m_current_neuron_number * 4, 0);
        m_p.resize(tpm.size()/3, std::vector<double>(3, 0));
        if (m_changing_tpm)
            m_p2.resize(tpm.size()/3, std::vector<double>(3, 0));
        m_switch_time.resize(m_st_neuron_num, std::vector<double>(m_st_stim_num, 0.3));
        std::vector<double> params;
        std::vector<double> ub;
        std::vector<double> lb;
        // rate
        for (int i = 0; i < m_current_neuron_number; ++i) {
            cont_vectors(params, rate);
            cont_vectors(ub, rate_u);
            cont_vectors(lb, rate_l);
        }
        // tpm
        cont_vectors(params, tpm);
        cont_vectors(ub, tpm_u);
        cont_vectors(lb, tpm_l);
        if (m_changing_tpm) {
            cont_vectors(params, get_tpm_from_all(tpm));
            cont_vectors(ub, get_tpm_from_all(tpm_u));
            cont_vectors(lb, get_tpm_from_all(tpm_l));
        }
        // time scale - exponential decay
        cont_vectors(params, std::vector<double>{1.0});
        cont_vectors(ub, std::vector<double>{1.0});
        cont_vectors(lb, std::vector<double>{1.0});
        // random switching time
        if (m_method == 2) {
            int num = m_st_neuron_num * m_st_stim_num;
            cont_vectors(params, std::vector<double>(num, 0.35));
            cont_vectors(ub, std::vector<double>(num, 0.5));
            cont_vectors(lb, std::vector<double>(num, 0.05));
        }
        // cif
        cont_vectors(params, cif);
        cont_vectors(ub, cif_u);
        cont_vectors(lb, cif_l);
        set_parameters(params);
        set_upper_bounds(ub);
        set_lower_bounds(lb);
        m_prob_condition = prob_condition;
        m_prob_index = prob_index;

        m_infer_rate = infer_rate;
        m_empirical_likelihood = emprical_likelihood;
    }

    void use_parallel() {
        m_method = 0;
    }

    void use_serial() {
        m_method = 1;
    }

    void use_random_switching() {
        m_method = 2;
    }

    void print_parameters(std::ofstream& oflikelihood, std::ofstream& ofprob,
                          std::ofstream& ofrate, std::ofstream& ofcif) {
        oflikelihood << m_opt_name << "\t" << m_min_f << "\t" << m_count << std::endl;
        ofprob << m_opt_name;
        for (auto& i : m_p) {
            for (auto& j : i) {
                ofprob << "\t" << j;
            }
        }
        for (auto& i : m_p2) {
            for (auto& j : i) {
                ofprob << "\t" << j;
            }
        }
        ofprob << std::endl;
        ofrate << m_opt_name;
        for (auto& i : m_rates) {
            ofrate << "\t" << i;
        }
        ofrate << std::endl;
        ofcif << m_opt_name;
        for (auto& i : mp_rk->get_parameters()) {
            ofcif << "\t" << i;
        }
        ofcif << std::endl;
    }

    void print_parameters(std::ofstream& oflikelihood, std::ofstream& ofprob,
                          std::ofstream& ofrate, std::ofstream& ofcif,
                          std::ofstream& ofswitch) {
        print_parameters(oflikelihood, ofprob, ofrate, ofcif);
        for (auto& i : m_switch_time) {
            ofswitch << m_opt_name;
            for (auto j : i) {
                ofswitch << "\t" << j;
            }
            ofswitch << std::endl;
        }
    }

protected:
    void cont_vectors(std::vector<double>& v,
            const std::vector<std::vector<double>>& vv) {
        for (auto& i : vv) {
            for (auto& j : i) {
                v.push_back(j);
            }
        }
    }

    void cont_vectors(std::vector<double>& v,
            const std::vector<double>& vv) {
        for (auto& j : vv) {
            v.push_back(j);
        }
    }

    void set_keys() {
        m_keys.clear();
        for (auto& trials : (*mp_all_data)) {
            m_keys[trials.first] = {};
            for (auto& mdv : trials.second) {
                m_keys[trials.first].push_back(mdv.first);
            }
        }
    }

    virtual int process_parameters() {
        int index = 0;
        for (int j = 0; j < m_current_neuron_number; ++j) {
            for (int i = 0; i < 4; ++i) {
                m_rates[index] = m_params[index];
                ++index;
            }
        }
        int i = index;
        for (size_t k = 0; k < m_p.size(); ++k) {
            for (int j = 0; j < 3; ++j) {
                m_p[k][j] = m_params[i+3*k+j];
            }
        }
        i += m_p.size() * 3;
        if (m_changing_tpm) {
            for (size_t k = 0; k < m_p2.size(); ++k) {
                for (int j = 0; j < 2; ++j) {
                    m_p2[k][j+1] = m_params[i+2*k+j];
                }
            }
            i += m_p2.size() * 2;
        }
        m_late_scale = m_params[i]; ++i;
        if (m_method == 2) {
            for (int j = 0; j < m_st_neuron_num; ++j) {
                for (int k = 0; k < m_st_stim_num; ++k) {
                    m_switch_time[j][k] = m_params[i+j*m_st_stim_num+k];
                }
            }
        }
        i += m_st_neuron_num * m_st_stim_num;
        return mp_rk->set_parameters(i, m_params);
    }

    std::vector<double> get_tpm_from_all(const std::vector<double>& all) {
        std::vector<double> tpm;
        for (size_t i = 1; i < all.size(); ++i) {
            if (i%3 != 0) tpm.push_back(all[i]);
        }
        return tpm;
    }

    double log_sum_exp(const std::vector<double>& logs) const {
        double log_max = logs[0];
        for (auto& i:logs) {
            if (i > log_max) log_max = i;
        }
        double sum = 0;
        for (auto& i:logs) {
            sum += exp(i - log_max);
        }
        return log_max + log(sum);
    }

    std::vector<double> m_rates;
    std::vector<std::vector<double>> m_p;
    std::vector<std::vector<double>> m_p2;
    double m_late_scale;
    AllData* mp_all_data;
    SumStat* mp_sum_stat;
    int m_current_neuron_number;
    std::map<std::string, std::vector<int>> m_keys; // filename -> {trialnum}

    bool m_infer_rate = false;
    bool m_empirical_likelihood = true;
    bool m_changing_tpm = false;

    int m_method = 0; // 0: parallel; 1: serial; 2: random switching
    std::vector<double> m_phase = {0.05, 0.2, 0.2, 0.35, 0.35, 0.5};
    std::vector<std::vector<Obj>> m_prob_condition;
    std::vector<int> m_prob_index;

    std::vector<std::vector<double>> m_switch_time;
    bool m_switch_time_diff_stimcond = true;
    bool m_switch_time_diff_neuron = true;
    int m_st_neuron_num;
    int m_st_stim_num;

}; // class CIFModel


} // namespace project

#endif //PROJECT_CIFMODEL_H
