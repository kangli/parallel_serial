#ifndef PROJECT_SERIALIZATION_H
#define PROJECT_SERIALIZATION_H

#include <project/NeuralData.h>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>

namespace boost {
namespace serialization {

template <class Archive>
void serialize(Archive & ar, project::DataMK& d, const unsigned int version) {
    ar & d.name;
    ar & d.index;
    ar & d.condition;
    ar & d.cue;
    ar & d.contra;
    ar & d.ipsi;
    ar & d.spike;
    ar & d.rate;
}

template <class Archive>
void serialize(Archive & ar, project::Summary& d, const unsigned int version) {
    ar & d.rate;
    ar & d.t_cont_rate;
    ar & d.t_ipsi_rate;
    ar & d.ni_cont_rate;
    ar & d.ni_ipsi_rate;
    ar & d.nc_cont_rate;
    ar & d.nc_ipsi_rate;
    ar & d.t_cont_epdf;
    ar & d.t_ipsi_epdf;
    ar & d.ni_cont_epdf;
    ar & d.ni_ipsi_epdf;
    ar & d.nc_cont_epdf;
    ar & d.nc_ipsi_epdf;
    ar & d.base_rate;
    ar & d.count;
}

template <class Archive>
void serialize(Archive & ar, project::EmpricalPDF& epdf, const unsigned int version) {
    ar & epdf.xs;
    ar & epdf.ys;
    ar & epdf.npd;
}

} // namespace serialization
} // namespace boost

namespace project {

// save an object to file using serialization
template <class O>
void save(const O& o, const std::string& filename) {
    std::ofstream ofs(filename);
    boost::archive::text_oarchive oa(ofs);
    oa << o;
}
template <class O>
void save(const O* o, const std::string& filename) {
    std::ofstream ofs(filename);
    boost::archive::text_oarchive oa(ofs);
    oa << *o;
}

// load an object from file using de-serialization
template <class O>
void load(O& o, const std::string& filename) {
    std::ifstream ifs(filename);
    boost::archive::text_iarchive ia(ifs);
    ia >> o;
}
// load an object from file using de-serialization
// FIXME not working
template <class O>
void load(O* o, const std::string& filename) {
    std::ifstream ifs(filename);
    boost::archive::text_iarchive ia(ifs);
    ia >> *o;
}

} // namespace project

#endif // PROJECT_SERIALIZATION_H
