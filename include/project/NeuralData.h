#ifndef PROJECT_NEURAL_DATA_H
#define PROJECT_NEURAL_DATA_H

#include <mass/Helper.h>

#include <matio.h>

#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>
#include <set>
#include <map>
#include <cmath>

namespace project {

enum Obj {
    OBJ_T = 0,
    OBJ_NI,
    OBJ_NC,
    OBJ_NONE,
};

struct ObjStim {
    std::string name;
    int neuron;         ///< neuron id, starting from 0
    Obj obj_cont;       ///< Obj
    Obj obj_ipsi;       ///< Obj
    int hemisphere;     ///< which hemisphere, 0, 1
    int phase;          ///< early or middle or late phase, 0, 1, 2
};

std::ostream& operator<<(std::ostream& out, const Obj value){
    const char* s = 0;
#define PROCESS_VAL(p) case(p): s = #p; break;
    switch(value){
        PROCESS_VAL(OBJ_T);     
        PROCESS_VAL(OBJ_NI);     
        PROCESS_VAL(OBJ_NC);
        PROCESS_VAL(OBJ_NONE);
    }
#undef PROCESS_VAL
    return out << s;
}

struct DataMK {
    std::string name;
    int index;
    int condition;
    int cue;
    Obj contra;
    Obj ipsi;
    std::vector<double> spike;
    std::vector<double> rate;
};

struct EmpricalPDF {
    double calc(double x) {
        double r = 1;
        if (xs.size() != 0) {
            int index = std::round( (x-xs.front()) * npd );
            int size = static_cast<int>(ys.size());
            if (index >= size) index = size-1;
            if (index < 0) index = 0;
            r = ys[index];
        }
        return r;
    }
    void print(std::string file) {
        std::ofstream of(file);
        for (size_t i = 0; i < xs.size(); ++i) {
            of << xs[i] << "\t" << ys[i] << "\n";
        }
    }
    std::vector<double> xs;
    std::vector<double> ys;
    double npd;
};

struct Summary {
    Summary() {
    }
    Summary(int n) {
        rate = std::vector<double>(n, 0);
        t_cont_rate = std::vector<double>(n, 0);
        t_ipsi_rate = std::vector<double>(n, 0);
        ni_cont_rate = std::vector<double>(n, 0);
        ni_ipsi_rate = std::vector<double>(n, 0);
        nc_cont_rate = std::vector<double>(n, 0);
        nc_ipsi_rate = std::vector<double>(n, 0);
        t_cont_epdf = std::vector<EmpricalPDF>(n);
        t_ipsi_epdf = std::vector<EmpricalPDF>(n);
        ni_cont_epdf = std::vector<EmpricalPDF>(n);
        ni_ipsi_epdf = std::vector<EmpricalPDF>(n);
        nc_cont_epdf = std::vector<EmpricalPDF>(n);
        nc_ipsi_epdf = std::vector<EmpricalPDF>(n);
        base_rate = 0;
        spike_train_num = 0;
    }
    std::vector<double> rate;
    std::vector<double> t_cont_rate;
    std::vector<double> t_ipsi_rate;
    std::vector<double> ni_cont_rate;
    std::vector<double> ni_ipsi_rate;
    std::vector<double> nc_cont_rate;
    std::vector<double> nc_ipsi_rate;
    std::vector<EmpricalPDF> t_cont_epdf;
    std::vector<EmpricalPDF> t_ipsi_epdf;
    std::vector<EmpricalPDF> ni_cont_epdf;
    std::vector<EmpricalPDF> ni_ipsi_epdf;
    std::vector<EmpricalPDF> nc_cont_epdf;
    std::vector<EmpricalPDF> nc_ipsi_epdf;
    double base_rate;
    int spike_train_num;
    int count = 0;
};

typedef std::map<int, std::vector<DataMK>> Trials;
typedef std::map<std::string, Trials> AllData;
typedef std::map<std::string, Summary> SumStat;

class NeuralData {
public:

    NeuralData() {
#if APPLE==1
        m_folder = "/Users/";
#else
        m_folder = "/home/";
#endif
    }

    void read_data(const std::string name, int index) {
        std::vector<std::string> filenames = get_file_list();
        std::map<std::string, std::vector<std::string>> file_map = 
            get_file_map(filenames);
        for (auto& fm : file_map) {
            if (fm.first != name) continue;
            Trials trials;
            m_all_data[fm.first] = trials;
            for (size_t i = 0; i < fm.second.size(); ++i) {
                if (index < 0 || index == static_cast<int>(i)) {
                    auto& file = fm.second[i];
                    read_one_file("../data/"+file+".mat", 
                            i, m_all_data[fm.first]);
                }
            }
        }
    }

    void read_data() {
        std::vector<std::string> filenames = get_file_list();
        std::map<std::string, std::vector<std::string>> file_map = 
            get_file_map(filenames);
        for (auto& fm : file_map) {
            //if (m_neuron_number[fm.first] < 5) continue;
            Trials trials;
            m_all_data[fm.first] = trials;
            for (size_t i = 0; i < fm.second.size(); ++i) {
                auto& file = fm.second[i];
                //std::cout << file << std::endl;
                read_one_file("../data/"+file+".mat", 
                              i, m_all_data[fm.first]);
            }
        }
    }

    void read_data(int length) {
        std::vector<std::string> filenames = get_file_list();
        std::map<std::string, std::vector<std::string>> file_map = 
            get_file_map(filenames);
        int count = 1;
        for (auto& fm : file_map) {
            Trials trials;
            m_all_data[fm.first] = trials;
            for (size_t i = 0; i < fm.second.size(); ++i) {
                auto& file = fm.second[i];
                //std::cout << file << std::endl;
                read_one_file("../data/"+file+".mat", 
                              i, m_all_data[fm.first]);
            }
            count++;
            if (count > length) break;
        }
    }

    std::vector<std::string> get_file_list() {
        std::string filename;
        std::vector<std::string> filenames;
        std::ifstream ifs("../data/MJ_MN_evmat_list.m");
        while (ifs >> filename) {
            filenames.push_back(filename);
        }
        return filenames;
    }

    std::map<std::string, std::vector<std::string>> get_file_map(
            const std::vector<std::string>& filenames) {
        std::map<std::string, std::vector<std::string>> file_map;
        for (auto& file : filenames) {
            std::string sub = file.substr(0, 8);
            if (file_map.count(sub) == 0) {
                file_map[sub] = {file};
            } else {
                file_map[sub].push_back(file);
            }
        }
        // record neuron numbers
        for (auto& fm : file_map) {
            m_neuron_number[fm.first] = fm.second.size();
        }
        return file_map;
    }


    AllData* get_data() { return & m_all_data; }
    SumStat* get_sum_stat() { return & m_sum_stat; }

    void set_data(const AllData& all_data, bool calc = true) { 
        m_all_data = all_data; 
        if (calc) {
            for (auto& ad : m_all_data) {
                calc_sum_stat(ad.second);
                calc_base_rate(ad.second);
            }
        }
    }
    void set_sumstat(const SumStat& ss) { m_sum_stat = ss; }
    void set_neuron_number(const std::map<std::string, int>& nn) { m_neuron_number = nn; }

    std::map<std::string, int>* get_neuron_number() { 
        return & m_neuron_number; 
    }

    void set_cue(int c) { m_cue = c; }

    void set_spike_num_thres(int t) { m_spike_num_thres = t; }

    void set_read_conditions(const std::vector<std::vector<Obj>>& c) {
        m_conditions = c;
    }

    void set_phase(const std::vector<double>& phase) {
        m_num_phase = phase.size() / 2;
        m_phase = phase;
    }

private:
    
    Obj get_obj(int o, int cue) {
        if (o == 5) {
            return OBJ_NC;
        } else if (o == 0) {
            return OBJ_NONE;
        } else {
            if (cue == 1) {
                if (o == 1) {
                    return OBJ_T;
                } else if (o == 4) {
                    return OBJ_NI;
                } else {
                    std::cout << "debug1" << std::endl;
                }
            } else if (cue == 2) {
                if (o == 2) {
                    return OBJ_T;
                } else if (o == 3) {
                    return OBJ_NI;
                } else {
                    std::cout << "debug3" << std::endl;
                }
            } 
        }
        return OBJ_NONE;
    }

    void read_one_file(const std::string& file, int index, Trials& trials) {
        mat_t* matfp;                         
        matvar_t* matvar;                        
        matfp = Mat_Open(file.c_str(), MAT_ACC_RDONLY); 
        if ( NULL == matfp ) {
            std::cout << "Error opening MAT file " << file << "!" << std::endl;
            return;
        }
        matvar_t* mat_event = NULL;
        matvar_t* mat_spike = NULL;
        while ( (matvar = Mat_VarReadNext(matfp)) != NULL ) {
            std::string name_str(matvar->name);
            if (name_str == "spike") {
                mat_spike = matvar;
            } else if (name_str == "event") {
                mat_event = matvar;
            } else {
                Mat_VarFree(matvar);
            }
            matvar = NULL;
        }
        Mat_Close(matfp);
        int N = mat_event->dims[0];
        double* event_d = static_cast<double*>(mat_event->data);
        // read all trials
        for (int i = 0; i < N; ++i) {
            int success = std::round(*(event_d + 3*N+i));
            int role = std::round(*(event_d + 8*N+i));
            if (success != 1 || role != 2) continue;
            int trial = std::round(*(event_d + i));
            int condition = std::round(*(event_d + N + i));
            int cue = std::round(*(event_d + 2*N + i));
            if (m_cue != 0 && cue != m_cue) continue;
            int o_cont = std::round(*(event_d + 4*N + i));
            int o_ipsi = std::round(*(event_d + 5*N + i));
            Obj obj_cont = get_obj(o_cont, cue);
            Obj obj_ipsi = get_obj(o_ipsi, cue);
            if (!is_satisfied(obj_cont, obj_ipsi)) continue;
            std::vector<double> spike;
            int num_spikes_inside = 0;
            std::vector<double> rate(m_num_phase, 0);
            matvar_t* spike_data = static_cast<matvar_t**>(mat_spike->data)[i];
            for (size_t j = 0; j < spike_data->dims[0]; ++j) {
                double* spike_time = static_cast<double*>(spike_data->data);
                double one_spike = 0.001 * spike_time[j];
                spike.push_back(one_spike);
                if (one_spike <= 0.5 && one_spike >= 0) {
                    num_spikes_inside++;
                }
                for (int l = 0; l < m_num_phase; ++l) {
                    if (one_spike >= m_phase[l*2] && one_spike < m_phase[l*2+1]) {
                        rate[l]++;
                    }
                }
            }
            for (int l = 0; l < m_num_phase; ++l) {
                rate[l] /= (m_phase[2*l+1]-m_phase[2*l]);
            }
            if (num_spikes_inside >= m_spike_num_thres) {
                DataMK dm{file, index, condition, cue, obj_cont, obj_ipsi,
                    spike, rate};
                if (trials.count(trial) == 0) {
                    trials[trial] = {dm};
                } else {
                    trials[trial].push_back(dm);
                }
            }
            while ((std::round(*(event_d+1+i)) == trial) && (i < N-1)) { ++i; }
        }
        calc_sum_stat(trials);
        calc_base_rate(trials);
        Mat_VarFree(mat_event);
        Mat_VarFree(mat_spike);
    }

    void calc_sum_stat(const Trials& trials) {
        struct RateTmp {
            std::vector<double> rate;
            std::vector<double> t_cont_rate;
            std::vector<double> t_ipsi_rate;
            std::vector<double> ni_cont_rate;
            std::vector<double> ni_ipsi_rate;
            std::vector<double> nc_cont_rate;
            std::vector<double> nc_ipsi_rate;
        };
        for (int i = 0; i < m_num_phase; ++i) {
            std::map<std::string, RateTmp> rate_map;
            for (auto& mdv : trials) {
                for (auto& md : mdv.second) {
                    if (rate_map.count(md.name) <= 0) {
                        rate_map[md.name] = RateTmp();
                    }
                    Obj cont = md.contra;
                    Obj ipsi = md.ipsi;
                    if (cont == OBJ_T && ipsi == OBJ_NONE) {
                        rate_map[md.name].t_cont_rate.push_back(md.rate[i]);
                    } else if (cont == OBJ_NI && ipsi == OBJ_NONE) {
                        rate_map[md.name].ni_cont_rate.push_back(md.rate[i]);
                    } else if (cont == OBJ_NC && ipsi == OBJ_NONE) {
                        rate_map[md.name].nc_cont_rate.push_back(md.rate[i]);
                    } else if (cont == OBJ_NONE && ipsi == OBJ_T) {
                        rate_map[md.name].t_ipsi_rate.push_back(md.rate[i]);
                    } else if (cont == OBJ_NONE && ipsi == OBJ_NI) {
                        rate_map[md.name].ni_ipsi_rate.push_back(md.rate[i]);
                    } else if (cont == OBJ_NONE && ipsi == OBJ_NC) {
                        rate_map[md.name].nc_ipsi_rate.push_back(md.rate[i]);
                    }

                }
            }
            for (auto& rm : rate_map) {
                if (m_sum_stat.count(rm.first) <= 0) {
                    m_sum_stat[rm.first] = Summary(m_num_phase);
                }
                double tmp = 0;
                tmp = mass::calc_mean(rm.second.rate);
                m_sum_stat[rm.first].rate[i] = (tmp == 0) ? 0.1 : tmp;
                tmp = mass::calc_mean(rm.second.t_cont_rate);
                m_sum_stat[rm.first].t_cont_rate[i] = (tmp == 0) ? 0.1 : tmp;
                tmp = mass::calc_mean(rm.second.t_ipsi_rate);
                m_sum_stat[rm.first].t_ipsi_rate[i] = (tmp == 0) ? 0.1 : tmp;
                tmp = mass::calc_mean(rm.second.ni_cont_rate);
                m_sum_stat[rm.first].ni_cont_rate[i] = (tmp == 0) ? 0.1 : tmp;
                tmp = mass::calc_mean(rm.second.ni_ipsi_rate);
                m_sum_stat[rm.first].ni_ipsi_rate[i] = (tmp == 0) ? 0.1 : tmp;
                tmp = mass::calc_mean(rm.second.nc_cont_rate);
                m_sum_stat[rm.first].nc_cont_rate[i] = (tmp == 0) ? 0.1 : tmp;
                tmp = mass::calc_mean(rm.second.nc_ipsi_rate);
                m_sum_stat[rm.first].nc_ipsi_rate[i] = (tmp == 0) ? 0.1 : tmp;

                m_sum_stat[rm.first].t_cont_epdf[i] = 
                    calc_kernel_density_function(rm.second.t_cont_rate);
                m_sum_stat[rm.first].t_ipsi_epdf[i] = 
                    calc_kernel_density_function(rm.second.t_ipsi_rate);
                m_sum_stat[rm.first].ni_cont_epdf[i] = 
                    calc_kernel_density_function(rm.second.ni_cont_rate);
                m_sum_stat[rm.first].ni_ipsi_epdf[i] = 
                    calc_kernel_density_function(rm.second.ni_ipsi_rate);
                m_sum_stat[rm.first].nc_cont_epdf[i] = 
                    calc_kernel_density_function(rm.second.nc_cont_rate);
                m_sum_stat[rm.first].nc_ipsi_epdf[i] = 
                    calc_kernel_density_function(rm.second.nc_ipsi_rate);
            }
        }

    }

    EmpricalPDF calc_kernel_density_function(const std::vector<double>& v) {
        EmpricalPDF epdf;
        if (v.size() > 0) {
            mass::calc_kernel_density_function(
                    mass::get_min(v), mass::get_max(v), 
                    m_epdf_n, m_epdf_cut, epdf.xs, epdf.ys, v);
            epdf.npd = m_epdf_n / (epdf.xs.back()-epdf.xs.front());
        }
        return epdf;
    }

    void calc_base_rate(const Trials& trials) {
        std::set<std::string> filenames;
        for (auto& mdv : trials) {
            for (auto& md : mdv.second) {
                filenames.insert(md.name);
                m_sum_stat[md.name].spike_train_num++;
                for (auto& i : md.spike) {
                    if (i < 0)
                        m_sum_stat[md.name].base_rate++;
        }   }   }
        for (auto& i : filenames) {
            m_sum_stat[i].base_rate /= (m_sum_stat[i].spike_train_num*0.4);
        }
    }

    bool is_satisfied(Obj left, Obj right) {
        bool b = false;
        if (m_conditions.size() == 0) {
            b = true;
        } else {
            for (auto& c : m_conditions) {
                if (c[0] == left && c[1] == right) {
                    b = true;
                    break;
                }
            }
        }
        return b;
    }

    AllData m_all_data;
    SumStat m_sum_stat;
    std::map<std::string, int> m_neuron_number;
    int m_cue = 1;
    int m_spike_num_thres = 2;
    std::vector<std::vector<Obj>> m_conditions;
    std::vector<double> m_phase;
    int m_num_phase;

    int m_epdf_n = 50;
    int m_epdf_cut = 3;

    std::string m_folder;

}; // class NeuralData

} // namespace project

#endif // PROJECT_NEURAL_DATA_H
