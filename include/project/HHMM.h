/*
 * The Binomial hidden Markov model.
 *
 */
#ifndef PROJECT_HHMM_H
#define PROJECT_HHMM_H

#include <project/Utils.h>
#include <project/NeuralData.h>
#include <mass/CIF.h>

#include <poibin/dftcf.h>

#include <Eigen/Dense>

#include <cmath>
#include <numeric>
#include <algorithm>

namespace project {

struct SimLatent {
    SimLatent() {}
    SimLatent(int neuronnum, int phasenum) {
        c = std::vector<int>(phasenum);
        x = std::vector<std::vector<int>>(neuronnum, std::vector<int>(phasenum));
    }
    std::vector<int> c;         // attentional state C
    std::vector<std::vector<int>> x; // neuronal attention X
};

// bionomial hidden markov model
class HHMM : public mass::CIF<ObjStim> {


public:

    HHMM(std::string name, mass::FuncType ft_rk, int n_rk, 
            mass::FuncType ft_pk, int n_pk)
    : CIF(name, ft_rk, n_rk, ft_pk, n_pk) {
        mm_ones << 1, 1, 1;
#if APPLE==1
        m_folder = "/Users/";
#else
        m_folder = "/home/";
#endif
    }

    HHMM(std::string name, AllData* p_ad, SumStat* p_ss,
            std::map<std::string, int>* p_m, mass::FuncType ft_rk, int n_rk, 
            mass::FuncType ft_pk, int n_pk)
    : CIF(name, ft_rk, n_rk, ft_pk, n_pk) {
        mm_ones << 1, 1, 1;
        set_parameter_names();
        mp_all_data = p_ad;
        mp_sum_stat = p_ss;
        set_keys();
        m_current_neuron_number = (*p_m)[name];
#if APPLE==1
        m_folder = "/Users/";
#else
        m_folder = "/home/";
#endif
    }

    void set_up(
            const std::vector<std::string>& files,
            const std::vector<double>& phase,
            const std::vector<std::vector<Obj>>& conditions,
            const std::vector<double>& lambda,
            const std::vector<double>& lambda_lb,
            const std::vector<double>& lambda_ub,
            const std::vector<double>& gamma,
            const std::vector<double>& gamma_lb,
            const std::vector<double>& gamma_ub,
            const std::vector<double>& A,
            const std::vector<double>& A_lb,
            const std::vector<double>& A_ub,
            const std::vector<double>& rates,
            const std::vector<double>& rates_lb,
            const std::vector<double>& rates_ub,
            double adapt, double adapt_lb, double adapt_ub,
            const std::vector<double>& cif,
            const std::vector<double>& cif_lb,
            const std::vector<double>& cif_ub
            ) {
        m_phase = phase;
        m_conditions = conditions;

        // relabel conditions
        relabel_conditions(*mp_all_data, m_conditions);
        //print_conditions(*mp_all_data);

        // initialize m_rates based on neuron number
        m_rates.resize(m_current_neuron_number*7, 0);

        // initialize mm_ps based on phase number
        int phase_num = phase.size() / 2;
        int cond_num = conditions.size();
        mm_ps.resize(phase_num);
        mm_gammas.resize(cond_num);
        mm_lambdas.resize(cond_num);

        // set initial parameters
        std::vector<double> params, lb, ub;
        // lambda
        cont_vectors(params, lambda);
        cont_vectors(lb, lambda_lb);
        cont_vectors(ub, lambda_ub);
        // Gamma
        cont_vectors(params, gamma);
        cont_vectors(lb, gamma_lb);
        cont_vectors(ub, gamma_ub);
        // A
        cont_vectors(params, A);
        cont_vectors(lb, A_lb);
        cont_vectors(ub, A_ub);
        // CIF
        // rate
        //std::vector<double> rate, rate_lb, rate_ub;
        //get_rate_params_from_data(files, rate, rate_lb, rate_ub);
        cont_vectors(params, rates);
        cont_vectors(lb, rates_lb);
        cont_vectors(ub, rates_ub);
        params.push_back(adapt);
        lb.push_back(adapt_lb);
        ub.push_back(adapt_ub);
        cont_vectors(params, cif);
        cont_vectors(lb, cif_lb);
        cont_vectors(ub, cif_ub);

        // set parameters and bounds
        set_parameters(params);
        set_lower_bounds(lb);
        set_upper_bounds(ub);
    }

    void set_up(
            const std::vector<std::string>& files,
            const std::vector<double>& phase,
            const std::vector<std::vector<Obj>>& conditions,
            const std::vector<double> params
            ) {
        m_phase = phase;
        m_conditions = conditions;

        // relabel conditions
        relabel_conditions(*mp_all_data, m_conditions);
        //print_conditions(*mp_all_data);

        // initialize m_rates based on neuron number
        m_rates.resize(m_current_neuron_number*7, 0);

        // initialize mm_ps based on phase number
        int phase_num = phase.size() / 2;
        int cond_num = conditions.size();
        mm_ps.resize(phase_num);
        mm_gammas.resize(cond_num);
        mm_lambdas.resize(cond_num);

        set_parameters(params);
    }

    void set_initial_flag(bool b) {
        m_same_initial = b;
    }

    virtual double calc_obj_func() const {
        double r = 0;
        for (auto& keys : m_keys) {
            for (size_t i = 0; i < keys.second.size(); ++i) {
                ObjStim objstim;
                auto& tmpmd = (*mp_all_data)[keys.first][keys.second[i]][0];
                if (tmpmd.condition > 16) continue;
                if (m_cue != 0 && tmpmd.cue != m_cue) continue;
                objstim.obj_cont = tmpmd.contra;
                objstim.obj_ipsi = tmpmd.ipsi;
                // make diagonal probability matrix
                int phase_num = m_phase.size() / 2;
                for (int j = 0; j < phase_num; j++) {
                    long double p1 = 1, p2 = 1, p3 = 1;
                    for (auto& md : (*mp_all_data)[keys.first][keys.second[i]]) {
                        objstim.hemisphere = 0;
                        objstim.neuron = md.index;
                        objstim.phase = j;
                        long double pps1 = exp(calc_spike_train_loglik(
                                objstim, md.spike, m_phase[j*2], m_phase[j*2+1]));
                        objstim.hemisphere = 1;
                        long double pps2 = exp(calc_spike_train_loglik(
                                objstim, md.spike, m_phase[j*2], m_phase[j*2+1]));
                        p1 *= m_a11*pps1 + (1-m_a11)*pps2;
                        p2 *= m_a21*pps1 + (1-m_a21)*pps2;
                        p3 *= m_a31*pps1 + (1-m_a31)*pps2;
                    }
                    mm_ps[j] << p1, 0, 0, 0, p2, 0, 0, 0, p3;
                }
                // calculate likelihood
                //Eigen::RowVector3d mat = mm_lambda * mm_ps[0];
                auto lambda = m_same_initial ? mm_lambda : mm_lambdas[tmpmd.condition];
                Eigen::Matrix<long double, 1, 3> mat = lambda * mm_ps[0];
                for (size_t j = 1; j < mm_ps.size(); ++j) {
                    mat = mat * get_gamma_from_cond(tmpmd) * mm_ps[j];
                }
                double ans = log(mat*mm_ones);
                if (ans < -1e10) ans = -1e10;
                else if (ans > 1e300) ans = 1e300;
                if (ans != ans) {
                    std::cout << "nan" << std::endl;
                }
                r += ans;
            }
        }
        return -r;
    }

    void simulate_spike_train(const ObjStim& stim, std::vector<double>& spike_train, double start, double end) const {
        std::mt19937 gen(rand());
        std::uniform_real_distribution<> dis(0, 1);
        // including start and end time points
        int size = std::round((end-start)/0.001);
        for (int i = 0; i <= size; ++i) {
            double prob = calc_cif(stim, spike_train, start+i*0.001) * 0.001;
            if (dis(gen) < prob) spike_train.push_back(start+i*0.001);
        }
    }

    AllData simulate_session(const std::string& session) {
        AllData new_data;
        ObjStim stim;
        new_data[session] = (*mp_all_data)[session];
        std::mt19937 gen(rand());
        std::uniform_real_distribution<> dis(0, 1);
        for (auto& trial : new_data[session]) {
            std::vector<double> cs(3);
            //Eigen::RowVector3d mat;
            Eigen::Matrix<long double, 1, 3> mat;
            cs[0] = dis(gen) < mm_lambda[0] ? 0 : 1;
            mat = mm_lambda * get_gamma_from_cond(trial.second[0]);
            cs[1] = dis(gen) < mat[0] ? 0 : 1;
            mat = mat * get_gamma_from_cond(trial.second[0]);
            cs[2] = dis(gen) < mat[0] ? 0 : 1;
            // simulate base
            for (size_t n = 0; n < trial.second.size(); ++n) {
                auto& dm = trial.second[n];
                stim.neuron = dm.index;
                stim.obj_cont = OBJ_NONE;
                stim.hemisphere = 0;
                dm.spike.clear();
                simulate_spike_train(stim, dm.spike, -0.4, m_phase[0]);
            }
            // simulate
            for (int t = 0; t < 3; ++t) {
                for (size_t n = 0; n < trial.second.size(); ++n) {
                    double r = cs[t] == 0 ? m_a11 : m_a21;
                    int x = dis(gen) < r ? 0 : 1;
                    auto& dm = trial.second[n];
                    stim.neuron = dm.index;
                    stim.obj_cont = dm.contra;
                    stim.obj_ipsi = dm.ipsi;
                    stim.hemisphere = x;
                    stim.phase = t;
                    simulate_spike_train(stim, dm.spike, m_phase[2*t], m_phase[2*t+1]);
                }
            }
        }
        return new_data;
    }

    AllData simulate_data(
            std::vector<std::vector<double>> gammas,
            std::vector<double> lambda,
            std::vector<std::vector<double>> A,
            std::vector<double> rates,
            double adapt,
            std::vector<double> weights,
            std::vector<double> phases,
            std::vector<std::vector<Obj>> conditions,
            int phasenum,
            int trialnum,
            int neuronnum) {
        mp_rk->set_parameters(0, weights);
        Trials trials;
        std::random_device rd;
        std::mt19937 gen(rd());
        ObjStim stim;
        stim.obj_cont = OBJ_T;
        stim.obj_ipsi = OBJ_NI;

        std::map<int, SimLatent> simlatent;

        // assign rates
        m_rates.resize(7*neuronnum);
        auto it = rates.begin();
        for (int j = 0; j < neuronnum; ++j) {
            for (int i = 0; i < 7; ++i) {
                m_rates[j*7+i] = *it;
                ++it;
            }
        }
        m_adapt = adapt;

        for (int i = 0; i < trialnum; ++i) {
            trials[i] = std::vector<DataMK>(neuronnum);
            SimLatent sl(neuronnum, phasenum);
            for (int j = 0; j < neuronnum; ++j) {
                trials[i][j].name = std::string("neuron") + std::to_string(j);
                trials[i][j].index = j;
                trials[i][j].condition = 0;
                trials[i][j].cue = 0;
                trials[i][j].contra = OBJ_T;
                trials[i][j].ipsi = OBJ_NI;
                trials[i][j].spike = {};
                trials[i][j].rate = {};
            }
            std::discrete_distribution<> d_disc(lambda.begin(), lambda.end());
            int c = d_disc(gen);
            for (int t = 0 ; t < phasenum; ++t) {
                stim.phase = t;
                if (t > 0) {
                    std::vector<double>& trans = gammas[c];
                    std::discrete_distribution<> d_disct(trans.begin(), trans.end());
                    c = d_disct(gen);
                }
                sl.c[t] = c;
                for (int j = 0; j < neuronnum; ++j) {
                    stim.neuron = j;
                    std::discrete_distribution<> d_discn(A[c].begin(), A[c].end());
                    int o = d_discn(gen);
                    stim.hemisphere = o;
                    simulate_spike_train(stim, trials[i][j].spike, phases[2*t], phases[2*t+1]);
                    sl.x[j][t] = o;
                }
            }
            simlatent[i] = sl;
        }
        AllData alldata = {{"sim", trials}};
        return alldata;
    }

    // simulate a group of simultaneously recorded spike trais
    // providing attention matrix, two rates and a unit interval
    AllData simulate_simultaneous_spike_trains(const std::vector<std::vector<int>>& attention, int unit_interval) {
        std::mt19937 gen(rand());
        std::uniform_real_distribution<> dis(0, 1);
        Trials trials;
        trials[0] = std::vector<DataMK>(0);
        size_t length = attention[0].size();
        for (size_t i = 0; i < attention.size(); ++i) {
            std::vector<double> spike_train;
            for (size_t j = 0; j < length; ++j) {
                int a = attention[i][j];
                ObjStim stim{"test", static_cast<int>(i), OBJ_T, OBJ_NC, a, static_cast<int>(j)};
                double start = j * unit_interval * 0.001 + 0.001;
                for (int k = 0; k < unit_interval; ++k) {
                    double prob = calc_cif(stim, spike_train, start+k*0.001) * 0.001;
                    double runif = dis(gen);
                    if (runif < prob) {
                        spike_train.push_back(start+k*0.001);
                    }
                }
            }
            DataMK dm {"test", static_cast<int>(i), 0, 1, OBJ_T, OBJ_NC, spike_train, {}};
            trials[0].push_back(dm);
        }
        AllData ad;
        ad["test"] = trials;
        return ad;
    }

    // structure of llk:
    // { { {h=0}, {h=1} }, { {h=0}, {h=1} }, ... , }
    //   ------- t=0 ---    -----t=1-------
    void decode_attention(std::ofstream* ofp = NULL) {

        int phase_num = m_phase.size() / 2;
        // Construct loglikelihood values of spike trains at different
        // hemisphere and different t
        std::map<int, std::vector<std::vector<std::vector<long double>>>> llk;
        ObjStim objstim;
        for (auto trial : mp_all_data->begin()->second) {
            int trialnum = trial.first;
            objstim.obj_cont = trial.second[0].contra;
            objstim.obj_ipsi = trial.second[0].ipsi;

            std::vector<std::vector<std::vector<long double>>> rs(phase_num);
            for (int j = 0; j < phase_num; j++) {
                objstim.phase = j;
                rs[j] = std::vector<std::vector<long double>>(2);
                for (auto& md : trial.second) {
                    objstim.neuron = md.index;
                    objstim.hemisphere = 0;
                    rs[j][0].push_back(calc_spike_train_loglik(
                            objstim, md.spike, m_phase[j*2], m_phase[j*2+1]));
                    objstim.hemisphere = 1;
                    rs[j][1].push_back(calc_spike_train_loglik(
                            objstim, md.spike, m_phase[j*2], m_phase[j*2+1]));
                }
            }
            llk[trialnum] = rs;
        }

        // Decode using llk and probabilities of HMM
        // { decode_t=1, decode_t=2,... }
        // First decode PC using forward-backward on HMM, then conditional on
        // PC, decode PX, finally calculate pmf using PC and PX|PC
        std::map<int, std::vector<Decode>> decs;
        for (auto& llktrial : llk) {
            std::vector<Decode> dec(phase_num);
            int trialnum = llktrial.first;
            auto& tmpmd = mp_all_data->begin()->second[trialnum][0];
            //Eigen::RowVector3d lam = mm_lambda;
            Eigen::Matrix<long double, 1, 3> lam = mm_lambda;
            int n = llktrial.second[0][0].size();
            // Forward
            for (int j = 0; j < phase_num; ++j) {
                if (j != 0) lam *= get_gamma_from_cond(tmpmd);
                std::vector<long double>& llk1 = llktrial.second[j][0];
                std::vector<long double>& llk2 = llktrial.second[j][1];
                dec[j].num = n;
                // PC = 1
                long double pc1 = lam[0];
                long double pdc1 = 1;
                for (int i = 0; i < n; ++i) {
                    pdc1 *= m_a11 * exp(llk1[i]) + (1-m_a11) * exp(llk2[i]);
                    // PX | PC
                    long double px1dc1 = exp(llk1[i]) * m_a11;
                    long double px2dc1 = exp(llk2[i]) * (1-m_a11);
                    dec[j].px_c1.push_back(px1dc1 / (px1dc1 + px2dc1));
                }
                long double pc1d = pc1 * pdc1;
                // debug
                //if (!std::isfinite(pc1d)) std::cout << "pc1d: " << pc1d << std::endl;

                // PC = 2
                long double pc2 = lam[1];
                long double pdc2 = 1;
                for (int i = 0; i < n; ++i) {
                    pdc2 *= m_a21 * exp(llk1[i]) + (1-m_a21) * exp(llk2[i]);
                    // PX | PC
                    long double px1dc2 = exp(llk1[i]) * m_a21;
                    long double px2dc2 = exp(llk2[i]) * (1-m_a21);
                    dec[j].px_c2.push_back(px1dc2 / (px1dc2 + px2dc2));
                }
                long double pc2d = pc2 * pdc2;
                // debug
                //if (!std::isfinite(pc2d)) std::cout << "pc2d: " << pc2d << std::endl;
                // PC = 3
                long double pc3 = lam[2];
                long double pdc3 = 1;
                for (int i = 0; i < n; ++i) {
                    pdc3 *= m_a31 * exp(llk1[i]) + (1-m_a31) * exp(llk2[i]);
                    // PX | PC
                    long double px1dc3 = exp(llk1[i]) * m_a31;
                    long double px2dc3 = exp(llk2[i]) * (1-m_a31);
                    dec[j].px_c3.push_back(px1dc3 / (px1dc3 + px2dc3));
                }
                long double pc3d = pc3 * pdc3;

                dec[j].pc1 = pc1d / (pc1d+pc2d+pc3d);
                dec[j].pc2 = pc2d / (pc1d+pc2d+pc3d);

                lam[0] = dec[j].pc1;
                lam[1] = dec[j].pc2;
                lam[2] = 1-dec[j].pc1-dec[j].pc2;
            }
            // Backward
            //Eigen::Vector3d pp, backp;
            Eigen::Matrix<long double, 3, 1> pp, backp;
            backp[0] = 1; backp[1] = 1; backp[2] = 1;
            for (int j = phase_num-2; j >= 0; --j) {
                std::vector<long double>& llk1 = llktrial.second[j+1][0];
                std::vector<long double>& llk2 = llktrial.second[j+1][1];

                long double pdc1_tp1 = 1;    // p(d_t+1 | c_t+1)
                long double pdc2_tp1 = 1;    // p(d_t+1 | c_t+1)
                long double pdc3_tp1 = 1;    // p(d_t+1 | c_t+1)
                for (int i = 0; i < n; ++i) {
                    pdc1_tp1 *= m_a11 * exp(llk1[i]) + (1-m_a11) * exp(llk2[i]);
                    pdc2_tp1 *= m_a21 * exp(llk1[i]) + (1-m_a21) * exp(llk2[i]);
                    pdc3_tp1 *= m_a31 * exp(llk1[i]) + (1-m_a31) * exp(llk2[i]);
                }
                pdc1_tp1 *= backp[0];
                pdc2_tp1 *= backp[1];
                pdc3_tp1 *= backp[2];

                pp[0] = pdc1_tp1; pp[1] = pdc2_tp1; pp[2] = pdc3_tp1;
                backp = get_gamma_from_cond(tmpmd) * pp;

                long double pc1 = dec[j].pc1 * backp[0];
                long double pc2 = (dec[j].pc2) * backp[1];
                long double pc3 = (1-dec[j].pc1-dec[j].pc2) * backp[2];

                dec[j].pc1 = pc1 / (pc1 + pc2 + pc3);
                dec[j].pc2 = pc2 / (pc1 + pc2 + pc3);
            }

            // calc pmf using PC and PX|PC
            for (int j = 0; j < phase_num; ++j) {
                auto pmf1 = poibin::dpoibin(dec[j].px_c1);
                auto pmf2 = poibin::dpoibin(dec[j].px_c2);
                auto pmf3 = poibin::dpoibin(dec[j].px_c3);
                long double w1 = dec[j].pc1;
                long double w2 = dec[j].pc2;
                long double w3 = 1-w1-w2;
                for (int i = 0; i <= n; ++i) {
                    long double tmp = pmf1[i] * w1 + pmf2[i] * w2 + pmf3[i] * w3;
                    dec[j].pmf.push_back(tmp);
                    if (i != n) dec[j].px.push_back(
                            dec[j].px_c1[i] * dec[j].pc1 + 
                            dec[j].px_c2[i] * (dec[j].pc2) +
                            dec[j].px_c3[i] * (1-dec[j].pc1-dec[j].pc2)
                            );
                }
                // calc Dn
                long double Dn = 0;
                for (int i = 0; i <= n; ++i) {
                    Dn += abs(i - 0.5*n) * dec[j].pmf[i];
                }
                dec[j].Dn = Dn / 0.5 / n;

                //debug
                if (dec[j].Dn > 1.01 || dec[j].Dn < -0.01) {
                    std::cout << "#Dn: " << dec[j].Dn << std::endl;
                }
            }
            decs[trialnum] = dec;
        }
        // output decoding
        if (ofp == NULL) {
            for (auto& d : decs) {
                int j = 0;
                for (auto& dd : d.second) {
                    std::cout << mp_all_data->begin()->first << "\t" << d.first << "\t" << mp_all_data->begin()->second[d.first][0].condition << "\t" << j << "\t" << m_cue << "\t" << dd.num << "\t" << dd.Dn << "\t" << dd.pc << "\t" << dd.pc1 << "\t" << dd.pc2 << "\t";
                    for (long double p : dd.pmf) std::cout << p << "\t";
                    for (long double p : dd.px) std::cout << p << "\t";
                    std::cout << "\n";
                    ++j;
                }
            }
        } else {
            for (auto& d : decs) {
                int j = 0;
                for (auto& dd : d.second) {
                    *ofp << mp_all_data->begin()->first << "\t" << d.first << "\t" << mp_all_data->begin()->second[d.first][0].condition << "\t" << j << "\t" << m_cue << "\t" << dd.num << "\t" << dd.Dn << "\t" << dd.pc << "\t" << dd.pc1 << "\t" << dd.pc2 << "\t";
                    for (long double p : dd.pmf) *ofp << p << "\t";
                    for (long double p : dd.px) *ofp << p << "\t";
                    *ofp << "\n";
                    ++j;
                }
            }
        }
    }

    Eigen::Matrix<long double, 3, 3> get_gamma_from_cond(const DataMK& dm) const {
        return mm_gammas[dm.condition];
    }

    virtual double calc_obj_func_decode() const {
        return 0;
    }

    virtual double calc_cif(const ObjStim& stim, const std::vector<double>& ss, 
            double t) const {
        double ada = t * m_adapt;
        int m = 0;
        for (auto i : ss) {
            if (i < t) m = i;
            else break;
        }
        double h = mp_rk->calc(m, t, ss);
        return calc_rate(stim, t) * exp(h + ada);
    }

    void get_rate_params_from_data(
            const std::vector<std::string>& files,
            std::vector<double>& rate,
            std::vector<double>& lb,
            std::vector<double>& ub ) const {
        std::string file_pre ="data/Data_set_MJ_MM/";
        std::string file_pos = ".mat";
        double rr = 1.0,
               rl = 0.2,
               ru = 5.0;
        for(size_t i = 0; i < files.size(); ++i) {
            std::vector<double> tmprs;
            std::string file = file_pre + files[i] + file_pos;
            tmprs.push_back(calc_mean((*mp_sum_stat)[file].t_cont_rate));
            tmprs.push_back(calc_mean((*mp_sum_stat)[file].ni_cont_rate));
            tmprs.push_back(calc_mean((*mp_sum_stat)[file].nc_cont_rate));
            tmprs.push_back(calc_mean((*mp_sum_stat)[file].t_ipsi_rate));
            tmprs.push_back(calc_mean((*mp_sum_stat)[file].ni_ipsi_rate));
            tmprs.push_back(calc_mean((*mp_sum_stat)[file].nc_ipsi_rate));
            for (double tmpr : tmprs) {
                if (std::isfinite(tmpr)) {
                    if (tmpr < 1) tmpr = 1;
                    rate.push_back(tmpr*rr);
                    lb.push_back(tmpr*rl);
                    ub.push_back(tmpr*ru);
                } else {
                    rate.push_back(10);
                    lb.push_back(1);
                    ub.push_back(60);
                }
            }
            double tmpr = (*mp_sum_stat)[file].base_rate;
            rate.push_back(tmpr);
            lb.push_back(tmpr*0.2);
            ub.push_back(tmpr*5.0);
        }
    }

    void randomize_params_mul(std::vector<double>& params, double l, double h) {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<> dis(l, h);
        for (auto& v : params) {
            v *= dis(gen);
        }
    }

    void randomize_params_set(std::vector<double>& params, double l, double h) {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<> dis(l, h);
        for (auto& v : params) {
            v = dis(gen);
        }
    }



    virtual void set_parameter_names() {
    }

    void print_parameters() {
        mass::CIF<ObjStim>::print_parameters();

        std::cout << "lambda:" << std::endl;
        std::cout << mm_lambda << std::endl;
        std::cout << std::endl;

        std::cout << "gammas:" << std::endl;
        for (auto& g : mm_gammas)
            std::cout << g << std::endl << std::endl;

        std::cout << "A:" << std::endl;
        std::cout << m_a11 << " " << 1 - m_a11 << "\n" << m_a21 << " " << 1 - m_a21 << "\n" << m_a31 << " " << 1-m_a31 << std::endl;
        std::cout << std::endl;

        std::cout << "rates:" << std::endl;
        for (auto r : m_rates) 
            std::cout << r << " ";
        std::cout << std::endl;
        std::cout << std::endl;

        std::cout << "cif:" << std::endl;
        for (auto r : mp_rk->get_parameters()) 
            std::cout << r << " ";
        std::cout << std::endl;
    }

    // cue = 0: use both 1 and 2
    void set_cue(int cue) { m_cue = cue; }


protected:

    void set_keys() {
        m_keys.clear();
        for (auto& trials : (*mp_all_data)) {
            m_keys[trials.first] = {};
            for (auto& mdv : trials.second) {
                m_keys[trials.first].push_back(mdv.first);
            }
        }
    }

    double calc_mean(const std::vector<double>& v) const {
        double r = 0;
        for (double i : v) {
            r += i;
        }
        return r*1.0/v.size();
    }

    template<class IT, class V>
    void softmax(IT& it, V& tmp) {
        std::transform(it, it+3, tmp.begin(), [](double v) {return exp(v);});
        double sum = std::accumulate(tmp.begin(), tmp.end(), 0.0);
        std::transform(tmp.begin(), tmp.end(), tmp.begin(), [sum](double v) {
            return v / sum;
        });
    }

    // parameters:
    //  - lambda: initial distribution of C
    //  - Gamma: tpm
    //  - A: a11, a21: attentional probability of X conditional on C
    //  - rate and history for point process
    virtual int process_parameters() {
        auto it = m_params.begin();

        std::vector<double> tmp(3);
        // lambda
        //mm_lambda << m_params[index], 1-m_params[index];
        if (m_same_initial) {
            softmax(it, tmp);
            mm_lambda << tmp[0], tmp[1], tmp[2];
            it += 3;
        } else {
            int cond_num = m_conditions.size();
            for (int i = 0; i < cond_num; ++i) {
                softmax(it, tmp);
                mm_lambdas[i](0,0) = tmp[0];
                mm_lambdas[i](0,1) = tmp[1];
                mm_lambdas[i](0,2) = tmp[2];
                it += 3;
            }
        }

        // tmp
        int cond_num = m_conditions.size();
        for (int i = 0; i < cond_num; ++i) {
            for (int j = 0; j < 3; ++j) {
                softmax(it, tmp);
                mm_gammas[i](j,0) = tmp[0];
                mm_gammas[i](j,1) = tmp[1];
                mm_gammas[i](j,2) = tmp[2];
                it += 3;
            }
        }

        // a11, a21, a31
        m_a11 = *it;
        m_a21 = *(it+1);
        m_a31 = *(it+2);
        it += 3;

        // rates
        for (int j = 0; j < m_current_neuron_number; ++j) {
            for (int i = 0; i < 7; ++i) {
                m_rates[j*7+i] = *it;
                ++it;
            }
        }

        // adapt
        m_adapt = *it; ++it;

        // response kernel
        return mp_rk->set_parameters(it-m_params.begin(), m_params);
    }

    // calculate the rate value for point process model
    virtual double calc_rate(const ObjStim& stim, double t) const {
        double rate;
        int i = stim.neuron * 7;
        Obj obj;
        int h = stim.hemisphere;
        if (h == 0) {
            obj = stim.obj_cont;
        } else {
            obj = stim.obj_ipsi;
        }
        switch (obj) {
            case OBJ_T:
                rate = h==0 ? m_rates[0+i] : m_rates[3+i];
                break;
            case OBJ_NI:
                rate = h==0 ? m_rates[1+i] : m_rates[4+i];
                break;
            case OBJ_NC:
                rate = h==0 ? m_rates[2+i] : m_rates[5+i];
                break;
            case OBJ_NONE:
                rate = m_rates[6+i];
                break;
            default:
                rate = m_rates[6+i];
        }
        /*
        switch (obj) {
            case OBJ_T:
                rate = m_rates[0+i];
                break;
            case OBJ_NI:
                rate = m_rates[1+i];
                break;
            case OBJ_NC:
                rate = m_rates[2+i];
                break;
            case OBJ_NONE:
                rate = m_rates[6+i];
                break;
            default:
                rate = m_rates[6+i];
        }
        */
        return rate;
    }

    void cont_vectors(std::vector<double>& v,
            const std::vector<double>& vv) {
        for (auto& j : vv) {
            v.push_back(j);
        }
    }


    // data
    AllData* mp_all_data;
    SumStat* mp_sum_stat;
    int m_current_neuron_number;
    std::map<std::string, std::vector<int>> m_keys; // filename -> {trialnum}

    // setup
    std::vector<double> m_phase;
    int m_cue = 1;
    std::vector<std::vector<Obj>> m_conditions;
    std::string m_folder;
    bool m_same_initial = true;

    // parameters
    std::vector<Eigen::Matrix<long double, 3, 3>> mm_gammas;  // tpm
    //Eigen::RowVector<long double, 3> mm_lambda; // initial distribution
    Eigen::Matrix<long double, 1, 3> mm_lambda; // initial distribution
    std::vector<Eigen::Matrix<long double, 1, 3>> mm_lambdas;  // initial distribution
    long double m_a11;     // nueronal attention
    long double m_a21;
    long double m_a31;
    std::vector<double> m_rates;
    double m_adapt;

    // intermediates
    //Eigen::Vector<long double, 3> mm_ones;
    Eigen::Matrix<long double, 3, 1> mm_ones;
    mutable std::vector<Eigen::Matrix<long double, 3, 3>> mm_ps;


public:
    //test
    void test() {
        std::vector<double> a = {1,2,3};
        std::vector<double> b(3);
        auto it = a.begin();
        softmax(it, b);
        std::cout << b[0] << " " << b[1] << " " << b[2] << std::endl;
    }

}; // class HHMM

} // namespace project

#endif // HHMM_H
