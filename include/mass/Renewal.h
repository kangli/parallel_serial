#ifndef MASS_RENEWAL_H
#define MASS_RENEWAL_H

#include <mass/Base.h>
#include <mass/Functions.h>

namespace mass {

template <class T>
class RenewalBase : public Base<T> {

public:
    RenewalBase(std::string name, FuncType dist, FuncType sk, int num_sk,
            FuncType pk, int num_pk) {
        this->mp_dist = create_isi_distribution(dist);
        this->mp_sk = create_stimulus_kernel(sk, num_sk);
        this->mp_pk = create_perception_kernel(pk, num_pk);
        this->m_opt_name = name;
    }
    ~RenewalBase() {}

    virtual double calc_spike_train_loglik(
            const T& stim,
            const std::vector<double>& st,
            double start, double end) const {
        //double rate = this->mp_sk->calc(0, stim);
        double rate = calc_rate(stim, 0);
        return this->mp_dist->calc_log_spike_train(start, end, st, rate);
    }

    virtual double calc_rate(const T& stim, double t) const {
        return 0;
    }

    virtual double calc_obj_func() const {
        double r = 0;
        /*
        size_t data_size = this->mp_stimuli->size();
#ifdef ENABLE_OPENMP
#pragma omp parallel for schedule (dynamic)
#endif
        for (size_t i = 0; i < data_size; ++i) {
            double tmp = calc_spike_train_loglik( (*(this->mp_stimuli))[i], (*(this->mp_spike_trains))[i], (*(this->mp_spike_trains))[i].front(), (*(this->mp_spike_trains))[i].back());
#ifdef ENABLE_OPENMP
#pragma omp atomic
#endif
            r += tmp;
        }
        */
        return -r;
    }

    virtual double calc_obj_func_decode() const {
        double r = 0;
        /*
        size_t data_size = this->m_stimuli.size();
#ifdef ENABLE_OPENMP
#pragma omp parallel for schedule (dynamic)
#endif
        for (size_t i = 0; i < data_size; ++i) {
            double tmp = calc_spike_train_loglik( this->m_stimuli[i], (*(this->mp_spike_trains))[i], (*(this->mp_spike_trains))[i].front(), (*(this->mp_spike_trains))[i].back());
#ifdef ENABLE_OPENMP
#pragma omp atomic
#endif
            r += tmp;
        }
        */
        return -r;
    }

    virtual void preprocess_spike_trains() {
    }

protected:
    virtual int process_parameters() {
        int i = this->mp_dist->set_parameters(0, this->m_params);
        i = this->mp_sk->set_parameters(i, this->m_params);
        return i;
    }

    std::shared_ptr<ISIDistribution> mp_dist;
    std::shared_ptr<StimulusKernel> mp_sk;
    std::shared_ptr<PerceptionKernel> mp_pk;
};


} // namespace mass

#endif // MASS_RENEWAL_H
