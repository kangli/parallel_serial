#ifndef MASS_BASE_H
#define MASS_BASE_H

#include <nlopt.hpp>

#include <vector>
#include <cmath>
#include <iostream>
#include <cstring>
#include <memory>
#include <omp.h>
#include <random>

namespace mass{

/******************************************************************************/
/**
 * \brief Base class of models.
 *
 * Template class T: Stimulus type, raw stimulus.
 * Template class S: Sub-Stimulus type, intermediate stimulus type.
 */
template <class T>
class Base {

public:
    Base() {}
    virtual ~Base() {}

    /**
     * \brief Calculate the log-likelihood value of a spike train.
     *
     * \param substim An intermediate stimulus input.
     * \param st Spike train.
     * \param start Starting time of spike train.
     * \param end Ending time of spike train.
     */
    virtual double calc_spike_train_loglik(
            const T& substim,
            const std::vector<double>& st,
            double start, double end) const = 0;

    /**
     * \brief Calculate objective function for optimization.
     *
     * Objective function is the loss function used in the model, for example 
     * the minus log-likelihood of the whole data.
     */
    virtual double calc_obj_func() const = 0;

    /**
     * \brief Calculate objective function for decoding.
     *
     * For decoding the objective function is dependent on different models.
     * The function takes the stimulus as parameters.
     */
    virtual double calc_obj_func_decode() const = 0;

    /**
     * \brief Set parameters for the model.
     *
     * Also process the parameters.
     */
    void set_parameters(const std::vector<double>& p) {
        m_params = p;
        process_parameters();
    }

    /**
     * \brief Print parameters to standard output.
     */
    void print_parameters() const {
        print_parameters_no_endl();
        std::cout << std::endl;
    }

    /**
     * \brief Print parameters to standard output.
     */
    void print_parameters_no_endl() const {
        std::cout << m_opt_name << "\t" << m_min_f << "\t" << m_count << "\t";
        for (std::size_t i = 0; i < m_params.size(); ++i) {
            std::cout << m_params[i] << "\t";
        }
    }

    /**
     * \brief Print decoding parameters (stimulus) to standard output.
     */
    void print_decoding_parameters() const {
        std::cout << m_opt_name << "-decoding\t" << m_min_f << "\t" << m_count << "\t";
        for (std::size_t i = 0; i < m_decoding_params.size(); ++i) {
            std::cout << m_decoding_params[i] << "\t";
        }
        std::cout << std::endl;
    }

    /**
     * \brief Print intermediate progress during optimization.
     *
     * Every 10000 loops, the parameters and the current objective function 
     * evaluation will be dumped to standard output.
     */
    void print_progress() const {
        if (m_count % 10000 > 0) {
            return;
        }
        std::cout << "*\t" << m_count << "\t" << m_f << "\t";
        for (double i : m_params) {
            std::cout << i << "\t";
        }
        std::cout << std::endl;
    }

    /**
     * \brief Print intermediate progress during decoding optimization.
     *
     * Same as print_progress.
     */
    void print_decoding_progress() const {
        if (m_count % 10000 > 0) {
            return;
        }
        std::cout << "*\t" << m_count << "\t" << m_f << "\t";
        for (double i : m_decoding_params) {
            std::cout << i << "\t";
        }
        std::cout << std::endl;
    }

    /**
     * \brief set local algorithm
     */
    void set_algorithm(nlopt::algorithm a) {
        m_algorithm = a;
    }

    /**
     * \brief set global algorithm
     */
    void set_algorithm_global(nlopt::algorithm a) {
        m_algorithm_global = a;
    }

    /**
     * \brief Return parameters.
     */
    std::vector<double> get_parameters() const {
        return m_params;
    }

    /**
     * \brief Return parameter names.
     */
    std::vector<std::string> get_parameter_names() const {
        return m_param_names;
    }

    /**
     * \brief Return decoding parameters (stimulus).
     */
    std::vector<double> get_decoding_parameters() const {
        return m_decoding_params;
    }

    /**
     * \brief Set lower boundary for optimization.
     */
    void set_lower_bounds(const std::vector<double>& p) {
        m_lb = p;
    }

    /**
     * \brief Set upper boundary for optimization.
     */
    void set_upper_bounds(const std::vector<double>& p) {
        m_ub = p;
    }

    /**
     * \brief Set stimuli.
     */
    void set_stimuli(std::vector<T>* p) {
        mp_stimuli = p;
    }

    /**
     * \brief Set spike trains.
     */
    void set_spike_trains(std::vector< std::vector<double> >* p) {
        mp_spike_trains = p;
    }

    /**
     * \brief Return pointer to stimuli.
     */
    std::vector<T>* get_stimuli() const {
        return mp_stimuli;
    }

    /**
     * \brief Return pointer to spike trains.
     */
    std::vector<std::vector<double>>* get_spike_trains() const {
        return mp_spike_trains;
    }

    /**
     * \brief Set maximal evaluation number.
     */
    void set_maxeval(int n) {
        m_maxeval = n;
    }

    /**
     * \brief Set maximal evaluation number for the global algorithm.
     */
    void set_maxeval_global(int n) {
        m_maxeval_global = n;
    }

    /**
     * \brief Set the name.
     */
    void set_opt_name(const std::string& name) {
        m_opt_name = name;
    }

    /**
     * \brief Set foler to store dumps.
     */
    void set_folder(const std::string& name) {
        m_folder = name;
    }

    /**
     * \brief Set (relative) tolerance value for optimization.
     */
    void set_ftol(double v) {
        m_ftol = v;
    }

    /**
     * \brief Get folder.
     */
    std::string get_folder() const {
        return m_folder;
    }

    /**
     * \brief Set decoding parameters (stimuli).
     *
     * Reconstruct the full decoding stimuli from the parameters.
     */
    virtual void set_decoding_parameters(const std::vector<double>& p) {
        m_decoding_params = p;
        set_stimuli_from_params();
    }

    /**
     * \brief Reconstruct the full decoding stimuli from the parameters.
     */
    virtual void set_stimuli_from_params() {}

    /**
     * \brief Get final objective function evaluation value.
     */
    double get_min_f() {
        return m_min_f;
    }

    /**
     * \brief Get iteration.
     */
    int get_count() {
        return m_count;
    }

    /**
     * \brief Set objective function evaluation value.
     */
    void set_f_val(double v) { m_f = v; }

    /**
     * \brief Loop count increases by 1.
     */
    void inc_counter() { m_count++; }

    /**
     * \brief Optimize the model.
     */
    virtual void optimize() {
        double f_min = 0;
        std::vector<double> init_params = m_params;
        if (m_maxeval_global > 0) {
            m_count = 0;
            run_opt(m_algorithm_global, m_maxeval_global, init_params, f_min);
            /*
            std::cout << "#global loop: " << m_count << std::endl;
            std::cout << "#init";
            for (auto v : init_params) {
                std::cout << "\t" << v;
            }
            std::cout << std::endl;
            */
        }
        if (m_maxeval > 0) {
            m_count = 0;
            run_opt(m_algorithm, m_maxeval, init_params, f_min);
        }
        m_min_f = f_min;
        set_parameters(init_params);
    }

    /**
     * \brief Decode parameters using the model.
     */
    virtual void decode() {
        double f_min = 0;
        std::vector<double> init_params = m_decoding_params;
        if (m_maxeval_global > 0) {
            m_count = 0;
            run_opt_decode(m_algorithm_global, m_maxeval_global, 
                           init_params, f_min);
            std::cout << "#global loop: " << m_count << std::endl;
            std::cout << "#init";
            for (auto v : init_params) {
                std::cout << "\t" << v;
            }
            std::cout << std::endl;
        }
        if (m_maxeval > 0) {
            m_count = 0;
            run_opt_decode(m_algorithm, m_maxeval, init_params, f_min);
        }
        m_min_f = f_min;
        set_decoding_parameters(init_params);
    }

    /**
     * \brief Optimization using MCMC (TODO).
     */
    void baye() {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::normal_distribution<> d(1.0, 0.1);
        this->set_parameters(this->m_params);
        int n = this->m_params.size();
        this->m_f = calc_obj_func();
        std::vector<double> min_params = m_params;
        for (int i = 0; i < this->m_maxeval; ++i) {
            this->m_params[i % n] += d(gen);
            this->set_parameters(this->m_params);
            double min_ff = this->calc_obj_func();
            this->m_count++;
            if (min_ff < this->m_f) {
                this->m_f = min_ff;
                min_params = this->m_params;
            }
            this->print_progress();
        }
    }

    /**
     * \brief Simulate a spike train.
     */
    void simulate(int stimindex, double dt, double len, size_t num, 
                  std::vector<double>& x, std::vector<double>& st) {
    }

    /**
     * \brief Simulate a spike train.
     */
    void simulate(int stimindex, double dt, double len, size_t num, 
                  std::vector<double>& st) {
    }

protected:
    /**
     * \brief Low level optimization.
     *
     * Create an nlopt object and optimize the model.
     * Use obj_func and this pointer to evaluate objective function.
     */
    void run_opt(nlopt::algorithm a, int m, std::vector<double>& p, 
                 double& min) {
        nlopt::opt optl(a, m_params.size());
        optl.set_lower_bounds(m_lb);
        optl.set_upper_bounds(m_ub);
        optl.set_min_objective(&Base::obj_func, this);
        optl.set_maxeval(m);
        optl.set_ftol_rel(m_ftol);
        optl.optimize(p, min);
    }
    /**
     * \brief Low level decoding.
     *
     * Create an nlopt object and decode using the model.
     */
    void run_opt_decode(nlopt::algorithm a, int m, std::vector<double>& p,
                        double& min) {
        nlopt::opt optl(a, m_decoding_params.size());
        optl.set_lower_bounds(m_lb);
        optl.set_upper_bounds(m_ub);
        optl.set_min_objective(&Base::obj_func_decode, this);
        optl.set_maxeval(m);
        optl.set_ftol_rel(m_ftol);
        optl.optimize(p, min);
    }

    /**
     * \brief Process parameters.
     *
     * Calculate necessary intermediate values from raw parameters.
     */
    virtual int process_parameters() = 0;

    /// pointer to stimuli.
    std::vector<T>* mp_stimuli;
    /// pointer to spike trains.
    std::vector< std::vector<double> >* mp_spike_trains;
    /// parameters.
    std::vector<double> m_params;
    /// parameter names.
    std::vector<std::string> m_param_names;
    /// lower bounds.
    std::vector<double> m_lb;
    /// upper bounds.
    std::vector<double> m_ub;
    /// decoding parameters.
    std::vector<double> m_decoding_params;
    /// decoding stimuli, obtained from m_decoding_parameters.
    std::vector<T> m_stimuli;       

    /// loop count.
    int m_count;
    /// name.
    std::string m_opt_name = "unnamed";
    /// final objective function evaluation value.
    double m_min_f;
    /// maximal evaluation number.
    int m_maxeval = 10000;
    /// maximal evaluation number for global optimization.
    int m_maxeval_global = 0;
    /// optimization algorithm.
    nlopt::algorithm m_algorithm = nlopt::LN_SBPLX;
    /// global optimization algorithm.
    nlopt::algorithm m_algorithm_global = nlopt::GN_DIRECT;
    //nlopt::algorithm m_algorithm_global = nlopt::GN_ISRES;
    /// intermediate objective function evaluation value.
    double m_f;
    /// relative optimization tolerance.
    double m_ftol = 1e-7;
    /// foler to store dumps.
    std::string m_folder = "out";

private:
    
    /**
     * \brief objective function.
     *
     * Using a model pointer to itself (this pointer) to run calc_obj_func.
     * Used inside run_opt.
     */
    static double obj_func(const std::vector<double>& params,
            std::vector<double>& grad, void* p_void) {
        Base* p_model = static_cast<Base*>(p_void);
        p_model->set_parameters(params);
        double v = p_model->calc_obj_func();
        p_model->set_f_val(v);
        p_model->inc_counter();
        p_model->print_progress();
        return v;
    }

    /**
     * \brief decoding objective function.
     */
    static double obj_func_decode(const std::vector<double>& params,
            std::vector<double>& grad, void* p_void) {
        Base* p_model = static_cast<Base*>(p_void);
        p_model->set_decoding_parameters(params);
        double v = p_model->calc_obj_func_decode();
        p_model->set_f_val(v);
        p_model->inc_counter();
        p_model->print_decoding_progress();
        return v;
    }

};



} // namespace mass

#endif // BASE_H
