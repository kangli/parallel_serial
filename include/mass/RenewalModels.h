#ifndef MASS_RENEWALMODELS_H
#define MASS_RENEWALMODELS_H

#include <mass/Renewal.h>

namespace mass {

class Renewal : public RenewalBase <std::vector<double>> {
public:
    Renewal(std::string name, FuncType dist, FuncType sk, int num_sk,
            FuncType pk, int num_pk)
        : RenewalBase(name, dist, sk, num_sk, pk, num_pk) {}
protected:
};


} // namespace mass

#endif // MASS_RENEWALMODELS_H
