#ifndef MASS_CIF_H
#define MASS_CIF_H

#include <mass/Base.h>
#include <mass/Functions.h>

namespace mass {

/******************************************************************************/
/**
 * \brief Point process models using Conditional Intensity Functions.
 */
template <class T>
class CIF : public Base<T> {

public:
    /**
     * \brief Constructor.
     *
     * \param name name of the model.
     * \param ft_rk 
     */
    CIF(std::string name, FuncType ft_rk, int n_rk, 
          FuncType ft_pk, int n_pk) {
        this->m_opt_name = name;
        this->mp_rk = create_response_kernel(ft_rk, n_rk);
        this->mp_pk = create_perception_kernel(ft_pk, n_pk);
    }

    /**
     * \brief Calculate conditional intensity function value.
     *
     * \param stim stimulus 
     * \param st spike trains
     * \param t current time
     */
    virtual double calc_cif(const T& stim, const std::vector<double>& st, 
                            double t) const = 0;

    /**
     * \brief Calculate the log-likelihood value of the spike train.
     */
    virtual double calc_spike_train_loglik(
            const T& stim,
            const std::vector<double>& st) const {
        double start = st.front();
        double end = st.back();
        return calc_spike_train_loglik(stim, st, start, end);
    }

    /**
     * \brief Calculate the log-likelihood value of the spike train.
     */
    virtual double calc_spike_train_loglik(
            const T& stim,
            const std::vector<double>& st,
            double start, double end) const {
        double result = 0;
        int max = round((end-start) / m_dt);
        for (int i = 0; i < max; ++i) {
            result -= calc_cif(stim, st, start+(i)*m_dt) * m_dt;
        }
        for (std::size_t i = 0; i < st.size(); ++i) {
            double t = st[i];
            if (t < start) continue;
            else if (t <= end) {
                result += log(calc_cif(stim, st, t));
                // DR2
                //result += 0.0005 * calc_cif(stim, st, t);
            } else {
                break;
            }
        }
        return result;
    }

    /**
     * \brief Perform time rescaling on a spike train.
     */
    virtual RescaledSpikeTrain perf_spike_train_time_rescaling(
            const T& stim,
            const std::vector<double>& st,
            double start, double end) const {
        double s = start;
        std::vector<double> rst;
        double first = start;
        double second = end;
        for (double spike : st) {
            if (spike > start && spike < end) {
                double second = spike;
                int max = round( (second - first) / m_dt );
                for (int j = 0; j < max; ++j) {
                    s += calc_cif(stim, st, first + (j)*m_dt) * m_dt;
                }
                first = second;
                rst.push_back(s);
            }
        }
        second = end;
        int max = round( (second - first) / m_dt );
        for (int j = 0; j < max; ++j) {
            s += calc_cif(stim, st, first + (j)*m_dt) * m_dt;
        }
        return {start, s, rst};
    }

    /**
     * \brief Calculate predicted firing times of a spike train.
     *
     * Does rescaling, and calculates ending time minus starting time.
     */
    double calc_spike_train_pred_fire_times(
            const T& stim,
            const std::vector<double>& st,
            double start, double end) const {
        RescaledSpikeTrain rst = perf_spike_train_time_rescaling(
                stim, st, start, end);
        return rst.end - rst.start;
    }

    virtual double calc_obj_func() const = 0;

    virtual double calc_obj_func_decode() const = 0;

protected:
    /// time discretization.
    double m_dt = 0.001;

    /// pointer to ResponseKernel.
    std::shared_ptr<ResponseKernel> mp_rk;
    /// pointer to PerceptionKernel.
    std::shared_ptr<PerceptionKernel> mp_pk;

};

}

#endif // CIF_H
