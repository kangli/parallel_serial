#ifndef MASS_CIFMODELS_H
#define MASS_CIFMODELS_H

#include <mass/Helper.h>
#include <mass/CIF.h>
#include <mass/Functions.h>

#include <boost/math/special_functions/gamma.hpp>

#include<cmath>
#include<iostream>

namespace mass {


/******************************************************************************/
/**
 * \brief Basic conditional intensity function model.
 */
class BasicCIF : public mass::CIF<std::vector<double>> {
public:

    /**
     * \brief Constructor.
     */
    BasicCIF(std::string name, mass::FuncType ft_rk, int n_rk, 
          mass::FuncType ft_pk, int n_pk)
          : CIF(name, ft_rk, n_rk, ft_pk, n_pk) {
        set_parameter_names();
    }

    virtual double calc_obj_func() const {
        double r = 0;
        size_t data_size = mp_stimuli->size();
#ifdef ENABLE_OPENMP
#pragma omp parallel for schedule (dynamic)
#endif
        for (size_t i = 0; i < data_size; ++i) {
            double tmp = calc_spike_train_loglik((*mp_stimuli)[i], 
                                                 (*mp_spike_trains)[i] );
#ifdef ENABLE_OPENMP
#pragma omp atomic
#endif
            r += tmp;
        }
        return -r;
    }

    virtual double calc_obj_func_decode() const {
        double r = 0;
        size_t data_size = m_stimuli.size();
#ifdef ENABLE_OPENMP
#pragma omp parallel for schedule (dynamic)
#endif
        for (size_t i = 0; i < data_size; ++i) {
            double tmp = calc_spike_train_loglik(m_stimuli[i], 
                                                (*mp_spike_trains)[i] );
#ifdef ENABLE_OPENMP
#pragma omp atomic
#endif
            r += tmp;
        }
        return -r;
    }

    virtual double calc_rate(const std::vector<double>& s, double t) const { 
        return m_base_rate;
    }

    virtual double calc_cif(const std::vector<double>& stim,
                            const std::vector<double>& ss, 
                            double t) const {
        double ada = stim.adapt() ? (t-stim.start_time()) * m_adapt : 0;
        int m = 0;
        for (auto i : ss) {
            if (i < t) m = i;
            else break;
        }
        double h = mp_rk->calc(m, t, ss);
        return calc_rate(stim, t) * exp(h + ada);
    }

    virtual void perf_uniformity_test(
            std::vector<double>& residuals_isi, 
            std::vector<double>& residuals_spikecount,
            std::vector<int>& cond_spikecount) const {
        size_t data_size = mp_stimuli->size();
        std::vector<mass::RescaledSpikeTrain> rsts;
        for (size_t i = 0; i < data_size; ++i) {
            rsts.push_back(perf_spike_train_time_rescaling(
                        (*mp_stimuli)[i], (*mp_spike_trains)[i] ));
            cond_spikecount.push_back( (*mp_stimuli)[i].cond );
        }
        std::vector<double> result_isi = perf_uniformity_test_isi(rsts);
        std::vector<double> result_spikecount 
            = perf_uniformity_test_spikecount(rsts);
        
        for (auto i : result_isi) {
            residuals_isi.push_back(i);
        }
        for (auto i : result_spikecount) {
            residuals_spikecount.push_back(i);
        }
    }

    void get_spike_train_conditions( std::vector<int>& conditions ) const {
        size_t data_size = mp_stimuli->size();
        for (size_t i = 0; i < data_size; ++i) {
            conditions.push_back((*mp_stimuli)[i].cond);
        }
    }

    virtual std::vector<double> calc_RMSD() {
        size_t n = mp_stimuli->size();
        double r = 0;
        for (size_t i = 0; i < n; ++i) {
            double start = (*mp_stimuli)[i].start;
            double end = (*mp_stimuli)[i].end;
            double time = end - start;
            double pred = calc_spike_train_pred_fire_times(
                    (*mp_stimuli)[i], (*mp_spike_trains)[i]);
            double emp = 0;
            for (auto tmp : (*mp_spike_trains)[i]) {
                if (tmp >= start && tmp <= end) emp = emp + 1;
            }
            r += (pred / time - emp / time) * (pred / time - emp / time);
        }
        r /= static_cast<double>(n);
        return {sqrt(r), static_cast<double>(n)};
    }

    void set_parameter_names() {
        auto rk_param_names = mp_rk->get_parameter_names();
        auto pk_param_names1 = mvp_pk[0]->get_parameter_names();
        auto pk_param_names2 = mvp_pk[1]->get_parameter_names();
        for (auto i : rk_param_names) {
            m_param_names.push_back(i);
        }
        for (auto i : pk_param_names1) {
            m_param_names.push_back(i);
        }
        for (auto i : pk_param_names2) {
            m_param_names.push_back(i);
        }
        auto other_param_names = {
            "r_0", "adapt", "w_atfix", "w_atin", "attend_1", "attend_2", 
            "attend_prefer", "w_uni_1", "w_uni_2", "lag"
        };
        for (auto i : other_param_names) {
            m_param_names.push_back(i);
        }
    }

protected:
    virtual int process_parameters() {
        int i = mp_rk->set_parameters(0, m_params);
        i = mvp_pk[0]->set_parameters(i, m_params);
        if (!m_use_one_tuning_curve) {
            i = mvp_pk[1]->set_parameters(i, m_params);
        }
        m_base_rate = m_params[i];
        ++i;
        m_adapt = m_params[i];
        ++i;
        m_weight_af = m_params[i];
        ++i;
        m_weight_ai = m_params[i];
        ++i;
        m_attend1 = m_params[i];
        ++i;
        m_attend2 = m_params[i];
        ++i;
        m_attend_prefer = m_params[i];
        ++i;
        m_weight_uni1 = m_params[i];
        ++i;
        m_weight_uni2 = m_params[i];
        ++i;
        m_lag = m_params[i];
        ++i;

        m_preferred_direction = static_cast<int>(
                round(mvp_pk[0]->get_parameters()[1] / M_PI * 180));
        if (m_preferred_direction < 0) {
            m_preferred_direction += 360;
        }

        return i;
    }

    virtual std::vector<double> perf_uniformity_test_isi(
            const std::vector<mass::RescaledSpikeTrain>& rsts) const {
        std::vector<double> residuals;
        for (auto& rst : rsts) {
            if (rst.st.size() == 0) {
                double res = (rst.end - rst.start);
                residuals.push_back(1-exp(-res));
                continue;
            }
            if (rst.start < rst.st.front()) {
                double res = (rst.st.front() - rst.start);
                residuals.push_back(1-exp(-res));
            }
            if (rst.end > rst.st.back()) {
                double res = (rst.end - rst.st.back());
                residuals.push_back(1-exp(-res));
            }
            size_t ds = rst.st.size();
            for (size_t i = 1; i < ds; ++i) {
                double res = (rst.st[i] - rst.st[i-1]);
                residuals.push_back(1-exp(-res));
            }
        }
        return residuals;
    }

    virtual std::vector<double> perf_uniformity_test_spikecount(
            const std::vector<mass::RescaledSpikeTrain>& rsts) const {
        std::vector<double> residuals;
        for (auto& rst : rsts) {
            int num = static_cast<int>(rst.st.size());
            double rate = (rst.end - rst.start);
            double r1 = boost::math::gamma_q(num+1, rate);
            if (num == 0) ++num;
            double r2 = boost::math::gamma_q(num, rate);
            residuals.push_back(0.5 * r1 + 0.5 * r2);
        }
        return residuals;
    }

    virtual void set_stimuli_from_params() {
    }

    int m_num_params;
    bool m_use_full_spike_train = false;
    bool m_use_one_tuning_curve = false;
    int m_preferred_direction;
    bool m_uni_probmix = false;

    std::vector<std::shared_ptr<mass::PerceptionKernel>> mvp_pk;
    double m_base_rate;
    double m_adapt;
    double m_weight_af;
    double m_weight_ai;
    double m_attend1;
    double m_attend2;
    double m_lag;
    double m_attend_prefer;
    double m_weight_uni1;
    double m_weight_uni2;
    
};

} // namespace mass

#endif // MASS_CIFMODELS_H
